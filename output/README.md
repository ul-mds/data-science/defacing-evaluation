
# Output Folders and Files meaning

## csv_files
Folder with all csv-files that are created throughout the project

### raw_data_step1
Folder with all csv-files that are created in the step1 of the pipeline.

* duplicate_files_nodefacedones.csv = Identified Duplicates
* filtered_dataset.csv = dataset after filtering for inclusion criteria
* LEUKODYS_2023-09-11.csv = generated overview file of all DICOM-attributes of the positive list (please note that the separator is the | !)

### raw_data_step3
Folder with all csv files created from step3 (feature collection and manual labelling)

* feature_collection_results.csv = CSV with the input features including metadata like filepaths etc. 
* manual_evaluation_FINALVERSION.csv = Results from the manual labelling process

### raw_data_step4
Folder with all csv files created from step4 (machine learning modelling)
* control_final_df_for_model_Defaced (lenient).csv = dataset used for the final machine learning modelling process for the lenient experiment
* control_final_df_for_model_Defaced (strict).csv = dataset used for the final machine learning modelling process for the strict experiment
* control_merged_features_and_labels_total_Defaced (lenient).csv = merged csv file of manual labelling and feature collection from step 3 (just lenient defacing column)
* control_merged_features_and_labels_total_Defaced (strict).csv = merged csv file of manual labelling and feature collection from step 3 (just strict defacing column)
* control_merged_features_both_strictnesses.csv = merged csv file of manual labelling and feature collection from step 3 (both columns of strict and lenient)

#### evaluating_model
> all_feat
> feat_red_1
> feat_red_2
> minim_feat

The subfolders contain the final scores of the performance metrics over 100 monte carlo experiments using the optimal classification threshold. They can be used for the creation of the visualisations. The FINAL_RESULTS tables show the meaned values for quick overview. 

### raw_data_step5
Folder contains some descriptives from the visualisation steps later. Not really relevant. 


## duplicates
Folder contains the identified and moved duplicate niftis and 2d images

## image_files_2d
Folder contains the 2D image files created from the 3D rendering process

## NiftiExaminations
Folder contains some experiments used for some visualisations for the thesis, and for the finding of ann appropriate threshold for the removal of artefacts.

## niftis
Folder contains all niftis that were downloaded from the filtered dataset from step1

## visuals
Folder contains visuals created for the final thesis. Some correlation heatmaps. 

### step4
Visuals from step 4 (machine learning modelling)
#### confusion_matrices
contains the confusion_matrices averaged (per feature set configuration)
#### feature_importances
contains the permutation feature importances (per feature set configuration)
#### metric_plots
contains the ROC Curves

### step 5
Visuals from step 5 (final visualisations of the results)
* Cohort descriptives
* histogram of values of certain features
* within model comparisons (comparison of feature sets of models)
* between model comparisons (comparison of different models' performances)