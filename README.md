## Defacing Evaluation

This repository contains source code for evaluating of the output quality of defacing software using machine learning.

### Before Usage 

> * Step 0: Set up virtual environment
> * pip3 install virtualenv
> * virtualenv venv_name
> * source venv_name/bin/activate
> * pip3 install -r requirements.txt

Set environment variables for XNAT connection:
export VARNAME=varvalue where VARNAME is XNAT_ADDRESS, etc. and varvalue is the string value. 


### **Data Import**
> Terminal Use from Model_Creation_Pipeline folder
  #### Generation Overview CSV 
    > First use: 
      > python3 src/main.py step1_overview

    > Continued use with unfinished csv file:
      > python3 src/main.py step1_overview --csv_overviewfile path_to_unfinished_csv_overview.csv
    

    > Nutzung mit eigenen Dicom Tags:
      > First use: 
        > python3 src/main.py step1_overview --csv_dicomtags individual_dicom_taglist.csv 

      > Continued use with unfinished csv file:
        > python3 src/main.py step1_overview --csv_dicomtags individual_dicom_taglist.csv --csv_overviewfile path_to_unfinished_csv_overview.csv

    Achtung: Die CSV hat den Separator: |
  #### Import Data
  > python3 src/main.py step1_import


### **Defacing**
  > Automatically uses Defacing Algorithms for Defacing the Temporary Niftis
  > python3 src/main.py step2

### **QualityControl**

  #### Get Features
    > python3 src/main.py step3_1

  #### Label Manually
    > python3 src/main.py step3_2

  #### Machine Learning Modelling
    > python3 src/main.py step4_model

### **Visuals**
  > python3 src/main.py step5_visuals

### Benchmarking
> Metriken
> * Accuracy
>   * Anzahl Correct Predicitons / Gesamt Prediction Anzahl
>   * == Anzahl der Übereinstimmungen Facedetection und Manuelle Bewertung bzgl. Face zu erkennen: Ja nein
> * Precision
>   * Anzahl der True Positives / Anzahl der positive Calls
>   * == Anzahl der FaceDetection Gesichter erkannt, die manuell auch als Gesichter erkannt wurden / Anzahl der von FaceDetection als Gesicht erkannten
> * Recall
>   * Anzahl der true Positives / Anzahl True Positives und False Negatives 
>   * == Anzahl der von FD erkannten Gesichter, die manuell auch erkannt werden / Anzahl der manuell als Gesicht erkannten Gesichter. 
> * F1 Score
>   * 2 * True Positive / 2 * TP + FP + FN 
> * Specificity
>   * Anzahl True Negatives / Anzahl Negatives 
>   * == Anzahl der nicht FD Bilder von nicht als Gesichter zu erkennenden Bilder / Anzahl aller nicht als Gesichter zu erkennenden Bilder
> * https://artemoppermann.com/de/accuracy-precision-recall-f1-score-und-specificity/

Für das Benchmarking wurden eine DICOM-Tag-CSV Datei erstellt. Diese Csv-Datei wurde gefiltert nach folgenden Kriterien:
  * Body Part Examined : Kopf / Skull / Head / ...
  * Series Description enthält: t1, t2, oder flair
  * MR Aqcuisition Type: 3D
  * Protocolname sollte kein "cor" enthalten (da bereits defacing vermutet)
  * Slice thickness <= 1.5
  * Spacing between Slices <= 5

# Open TODOs
* CSV Dateinamen parametrisieren
* Doc String Kommentare
* Clean up Code
* Make code more efficient


NOTE: The usage description has not been tested on a new computer, there might be unforseen issues. 