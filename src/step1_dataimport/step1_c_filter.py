import pandas as pd
from util import config_vars
import os

class FilterDataset:
    """filters the overview-csv-file according to inclusion criteria and returns tuples of subject-, session-, and scan-ids for each MRI-scan.
    """
    def __init__(self, csv_path:str) -> None: 
        self._csv_path = csv_path

    def filter_dataset_for_model(self) -> pd.DataFrame:
        """filters overview-csv file according to filters 1-6. 
        Bodypart should be the head, it should be T1, T2, or Flair. 
        It should be 3D. 
        It should have a slice thickness of no more than 1.5.
        It should have spacing between slices of no more than 5. 

        Returns:
            pd.DataFrame: filtered dataframe
        """
        df =  pd.read_csv(self._csv_path, delimiter='|')
        filter_1 = df['Body Part Examined'].isin(['BRAIN', 'HEAD','CP HEAD', 'KOPF', 'SKULL', 'HEADNECK', 'MR Schädel', 'MRT des Gehirns', 'Schädel-CT'])
        filter_2 = df['Series Description'].str.contains('flair|t1|t2', case = False, na=False)
        filter_3 = df['MR Acquisition Type']=='3D'
        filter_5 = df['Slice Thickness'].apply(lambda x: float(x) if x != "None" else pd.NA) <= 1.5 #evtl. sollte das mit dem None eh schon vorher gehandled sein, als jetzt, also nochmal schauen TODO
        filter_6 = df['Spacing Between Slices'].astype(float) <= 5 
        filter_4 = ~df['Protocol Name'].str.contains('cor', case = False, na = False)
        df_filtered = df[filter_1 & filter_2 & filter_3 & filter_4 & filter_5 & filter_6]
        df_filtered.to_csv(os.path.join(config_vars.CSV_PATH_STEP1,'filtered_dataset.csv'))
        return df_filtered

    def create_filtered_idtuples_for_xnat(self, df:pd.DataFrame) -> list:
        id_tuples = []
        for _, row in df.iterrows():
            id_tuple = tuple(row[:3])
            id_tuples.append(id_tuple)
        return id_tuples

    def run(self) -> list:
        """ applies filters to the overview-csv and creates tuples of this filtered dataset for the subsequent import step. 

        Returns:
            list: list of id-tuples
        """
        df = self.filter_dataset_for_model()
        id_tuples = self.create_filtered_idtuples_for_xnat(df)
        return id_tuples
