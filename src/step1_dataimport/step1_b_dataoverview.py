import pandas as pd
import os
from util import config_vars
import datetime
import csv
from step1_dataimport.step1_a_connection import XNATConnectionProvider

class DataOverview:
    """ generates a csv file filled with the subjects-, session-, and scan-ids of the MRI scans in the xnat-database. 
    Reads the dicom-tags given by the dicom-tag-csv of each MRI-Scan and saves the respective information in the generated overview-csv.
    """
    def __init__(self, tag_csv:str, project_name:str, connect_provider:XNATConnectionProvider, csv_file:None|str = None) -> None: #FIXME dieses bool str ding ist nicht so nice geregelt.
        self._tag_df =  pd.read_csv(tag_csv, header=None)
        self._tag_list = self._tag_df[0].tolist() #evtl. das hier beides zusammen?!
        self._project_name = project_name
        self._connect_provider = connect_provider
        self._output_dir = config_vars.CSV_PATH_STEP1
        self._headers = ['subject_id', 'session_id', 'scan_id'] + self._tag_list 
        if csv_file is None:
            self._csv = os.path.join(self._output_dir,f"{self._project_name}_{str(datetime.datetime.now().strftime('%Y-%m-%d'))}.csv")
        else:
            self._csv = csv_file

    def create_csv(self):
        """if the csv-overview does not yet exist, it will be generated. 
        """
        if not os.path.exists(self._csv):
            with open(self._csv, 'w', encoding= 'utf-8') as csvfile:
                writer = csv.DictWriter(csvfile, quotechar = '"', delimiter = '|', escapechar='\\', fieldnames=self._headers) #TODO : evtl. replace mit backslash???
                writer.writeheader() 

    def fill_csv_with_data(self) -> None:
        """goes through subjects, sessions and scans and reads the dicom-tags. If the dicom-tag is in the tag-list, it will write its content to the csv file. 
        """
        # TODO make better dataset with no backslashes before the commas!
        csv_df = pd.read_csv(self._csv, delimiter='|')
        existing_combo = set(zip(csv_df['subject_id'], csv_df['session_id'], csv_df['scan_id']))

        with self._connect_provider.create_connection() as connection:
            project = connection.projects[self._project_name].subjects
            for subject_id in project:
                sessions = project[subject_id].experiments
                for session in sessions:
                    scans = sessions[session].scans
                    for scan_id, scan in scans.items():
                        if (subject_id, session, scan_id) in existing_combo:
                            continue
                        row_dict = {'subject_id':subject_id, 'session_id':session, 'scan_id':scan_id}
                        try:
                            dicomread = scan.read_dicom()
                            for tag in self._tag_list: #TODO loopdepth fix - is it possible with less loopies?
                                found = False  
                                for element in dicomread:
                                    if tag == element.name:
                                        found = True
                                        row_dict[tag] = str(element.value).replace("'", '') # NOTE is it really important? Check it sometime again. maybe it changed?
                                if not found: 
                                    row_dict[tag] = pd.NA
                            with open(self._csv, 'a', encoding='utf-8') as csvfile:
                                writer = csv.DictWriter(csvfile, quotechar = '"', delimiter = '|', escapechar='\\', fieldnames=self._headers)
                                writer.writerow(row_dict)
                        except Exception as e: #logging exceptions and warnings
                            print(e) #TODO evtl. eines tages nicht mehr printen? Sondern akzeptieren dass es nur in log eingesehen wird? #TODO sollte man noch special Exceptions abfangen, damit andere Exception noch Errors machen?

    def run(self) -> str:
        """ creates a csv if it doesnt exist and fills it with data. If the csv exists, it will continue where it was left off. 

        Returns:
            str: path to the csv-file
        """
        self.create_csv()
        self.fill_csv_with_data()
        return self._csv
