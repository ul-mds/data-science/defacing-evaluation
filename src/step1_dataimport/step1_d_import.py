import os
import shutil
from util import config_vars
from util.io import make_dir_if_not_exists
from step1_dataimport.step1_a_connection import XNATConnectionProvider
from step1_dataimport.nifti_conversion import conversion_dicom_to_nifti_singleimage

class DataImport:
    """ Imports the MRI-data as dicoms using specified subject-, session-, and scan-ids. Converts those dicoms to niftis and deletes the dicom immediately. 
    """
    def __init__(self, project_name, connect_provider:XNATConnectionProvider) -> None:
        self._project_name = project_name
        self._connect_provider = connect_provider
        self._output_dir = config_vars.NIFTI_PATH #CHECK THIS, IF THIS WORKS (CHANGED IT!!!! TODO)

    def download(self, subj_id:str, session_id:str, scan_id:str) -> str:
        """ imports the dicoms of the MRI-Scans of the given subj-id, sess-id and scan-id combination.

        Args:
            subj_id (str): subject-id
            session_id (str): session-id
            scan_id (str): scan-id

        Returns:
            str: dicom-directory path for subsequent nifti-conversion and cleanup
        """
        with self._connect_provider.create_connection() as connect:
            dicom_dir_name = os.path.join(self._output_dir, f'{subj_id}_{session_id}_{scan_id}')
            xnat_scan = connect.subjects[subj_id].experiments[session_id].scans[scan_id] 
            print(f'Processing {dicom_dir_name}')
            if not os.path.isdir(f'{dicom_dir_name}_NIFTI') or len(os.listdir(f'{dicom_dir_name}_NIFTI')) == 0:
                make_dir_if_not_exists(dicom_dir_name)
                if len(os.listdir(dicom_dir_name)) == 0:
                    xnat_scan.download_dir(dicom_dir_name)
            return dicom_dir_name

    def cleanup(self, dicom_dir_name: str) -> None:
        """ removes the dicom-directories after conversion to niftis. 

        Args:
            dicom_dir_name (str): the dir path of the dicom
        """
        try:
            shutil.rmtree(dicom_dir_name)
        except FileNotFoundError:
            pass

    def run(self, subj_id:str, session_id:str, scan_id:str) -> None:
        dicom_dir_name = self.download(subj_id, session_id, scan_id)
        conversion_dicom_to_nifti_singleimage(dicom_dir_name)
        self.cleanup(dicom_dir_name)