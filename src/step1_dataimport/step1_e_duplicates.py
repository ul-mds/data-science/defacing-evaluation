import os
import shutil
import csv
import pandas as pd
import hashlib

from util import config_vars
from util.io import make_dir_if_not_exists

class IdentifyAndMoveDuplicates:
    """ There can be duplicates in the data that was downloaded. 
    Those duplicates are identified and moved into a special duplicates folder, to take them out of the following steps.
    """
    def __init__(self, output_csv: str= os.path.join(config_vars.CSV_PATH_STEP1,"duplicate_files_nodefacedones.csv")) -> None:
        self.root_dir = config_vars.NIFTI_PATH
        self.output_csv = output_csv

    def file_hash(self, file_path):
        """
        Calculates the hash of a file.
        """
        sha1 = hashlib.sha1()
        with open(file_path, "rb") as f:
            while True:
                data = f.read(65536)  # Read the file in 64k chunks
                if not data:
                    break
                sha1.update(data)
        return sha1.hexdigest()

    def find_duplicate_files(self, output_csv:str) -> None:
        """
        Find duplicate files within a folder structure and save results to a CSV file.
        """
        # Create a dictionary to store file hashes and paths
        file_hash_dict = {}
        duplicates = []

        for folder_name, _, files in os.walk(self.root_dir):
            for file_name in files:
                if 'mrideface' in file_name or 'pydeface' in file_name or 'quickshear' in file_name or 'brainmask' in file_name:
                    continue
                else:
                    file_path = os.path.join(folder_name, file_name)
                    file_hash_value = self.file_hash(file_path)

                    # Check if the hash already exists in the dictionary
                    if file_hash_value in file_hash_dict:
                        original_file = file_hash_dict[file_hash_value]
                        duplicates.append((original_file, file_path))
                        self.move_duplicates_to_other_dir(file_path)
                    else:
                        file_hash_dict[file_hash_value] = file_path

        # Save duplicate information to a CSV file
        with open(output_csv, 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow(['Original', 'Duplicate'])
            csv_writer.writerows(duplicates)

    def move_duplicates_to_other_dir(self,file_path:str):
        """  TODO

        Args:
            file_path (str): filepath of duplicate to move. or something FIXME
        """
        print(file_path)
        file_dest_dir = file_path.replace(f'{self.root_dir}/', '') 
        xnat_id_dir = os.path.dirname(file_dest_dir)

        file_dir = (os.path.dirname(file_path))    
        destination_dir = os.path.join(f'{config_vars.DUPL_PATH}/duplicate_niftis',xnat_id_dir)
        make_dir_if_not_exists(destination_dir) #check functionality #FIXME DELETE
        shutil.move(file_dir, destination_dir)
        # do this with imagefiles as well! 
        try:
            file_dir_img = os.path.dirname(file_path.replace('niftis', 'image_files_2d'))
            print(file_dir_img)
            destination_dir_img = os.path.join(f'{config_vars.DUPL_PATH}/duplicate_imgs', xnat_id_dir)
            destination_dir_img = 'output/duplicates/duplicate_imgs'
            make_dir_if_not_exists(destination_dir_img) # check functionality #FIXME DELETE
            shutil.move(file_dir_img, destination_dir_img)
        except Exception as e:
            print(e)
    
    def stats(self):
        """ generates a small print statement of the number of scans that have duplicates and the total scans that were moved in response. 
        """
        duplicates_df = pd.read_csv(self.output_csv)
        kept_originals = duplicates_df['Original']
        uniq = kept_originals.unique()
        print(f'Duplicate Deletion in Numbers: {len(uniq)} scans had duplicates. Total nr of scans moved: {len(kept_originals)}')

    def run(self):
        self.find_duplicate_files(self.output_csv)
        self.stats()
