import dicom2nifti
import os
import pydicom
from pydicom.errors import InvalidDicomError
from dicom2nifti.exceptions import ConversionValidationError

def file_is_dicom(filename:str) -> bool:
    """ check whether file is dicom

    Args:
        filename (str): path string of file to check

    Returns:
        bool: true if file is dicom, else false
    """
    try:
        pydicom.dcmread(filename)
        return True
    except InvalidDicomError:
        return False


def conversion_dicom_to_nifti_singleimage(dicom_output_path:str) -> None:
    """ converts a dicom folder of a single scan into a nifti
        if the file is already converted, it will be skipped
        if there is a different exception it will check whether 
        there are non-dicoms and further investigation is needed

    Args:
        dicom_output_path (str): path string of the dicom folder
    """
    nifti_dir_name = f'{dicom_output_path}_NIFTI' 
    if not os.path.isdir(nifti_dir_name): #Check if this is even necessary or can be removed? TODO
        os.mkdir(nifti_dir_name)

    if len(os.listdir(nifti_dir_name)) == 0:  
        try:
            dicom2nifti.convert_directory(dicom_output_path, nifti_dir_name)
            print('Dicom to Nifti Conversion successful: ', nifti_dir_name)
            return nifti_dir_name
        except Exception as e:
            print('An Exception occured during Conversion of Dicom to Nifti: ', e)