import xnat

class XNATConnectionProvider:
    """ Connects to XNAT-server with the given alias, secret and address
    """
    def __init__(self, xnat_alias, xnat_secret, xnat_web_addr ):
        self._xnat_alias = xnat_alias
        self._xnat_secret = xnat_secret
        self._xnat_web_addr = xnat_web_addr

    def create_connection(self):
        """creates and returns a connection to the xnat_web_addr if the login is successful. 

        Returns:
            xnat-connection: the connection to the XNAT-Server
        """
        try:
            xnat_connection = xnat.connect(server=self._xnat_web_addr, user = self._xnat_alias, password= self._xnat_secret)
            print('Login succeeded!')
            return xnat_connection
        except xnat.exceptions.XNATLoginFailedError as e:
            print('xnat.exceptions.XNATLoginFailedError - Login failed. Please make new token and insert the credentials!', e)

