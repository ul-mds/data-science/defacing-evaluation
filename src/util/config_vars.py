import yaml 
import os

with open('src/util/config.yml', 'r') as yml_config:
    config = yaml.safe_load(yml_config)

# defacing scripts
QS_SCRIPT = config['defacing_script_locations']['quickshear']['script']
MRI_DEF_SCRIPT = config['defacing_script_locations']['mrideface']['script']
MRI_DEF_TALAIRACH = config['defacing_script_locations']['mrideface']['talairach']
MRI_DEF_FACE = config['defacing_script_locations']['mrideface']['face']

# path variables
NIFTI_PATH = config['paths']['niftis']
CSV_PATH_GENERAL = config['paths']['csv_general']
CSV_PATH_SUBSTEPS = config['paths']['csv_substeps']
CSV_PATH_STEP1 = os.path.join(CSV_PATH_GENERAL, CSV_PATH_SUBSTEPS['step1'])
CSV_PATH_STEP3 = os.path.join(CSV_PATH_GENERAL, CSV_PATH_SUBSTEPS['step3'])
CSV_PATH_STEP4 = os.path.join(CSV_PATH_GENERAL, CSV_PATH_SUBSTEPS['step4'])
CSV_PATH_STEP5 = os.path.join(CSV_PATH_GENERAL, CSV_PATH_SUBSTEPS['step5'])

IMAGES_PATH = config['paths']['images']
DUPL_PATH = config['paths']['dupl']
VISUALS_PATH = config['paths']['visuals_general']
VISUALS_SUBSTEPS = config['paths']['visuals_substeps']
VISUALS_STEP1 = os.path.join(VISUALS_PATH, VISUALS_SUBSTEPS['step1'])
VISUALS_STEP2 = os.path.join(VISUALS_PATH, VISUALS_SUBSTEPS['step2'])
VISUALS_STEP3 = os.path.join(VISUALS_PATH, VISUALS_SUBSTEPS['step3'])
VISUALS_STEP4 = os.path.join(VISUALS_PATH, VISUALS_SUBSTEPS['step4'])
VISUALS_STEP5 = os.path.join(VISUALS_PATH, VISUALS_SUBSTEPS['step5'])
