import os
import logging

def make_dir_if_not_exists(pathstring): #pathstring sollte #das hier ist unnötig sobald ich alles als stream mache, dann mache ich einfach aus prinzip ein directory. 
    totalpath = os.path.join(os.getcwd(), pathstring)
    if not os.path.isdir(totalpath):
        try:
            os.makedirs(totalpath)
            return totalpath
        except Exception as e:
            #logging.exception('An exception occured: ', e)
            print(e)

def logger():
    pass

class MissingEnvironmentVariables(Exception):#FIXME doesnt seem to print this message, maybe just i dont know.. 
    print("One or more of the following environment variables are not set but need to be set: XNAT_ALIAS, XNAT_ADDRESS, XNAT_SECRET, XNAT_PROJECT. Set them by typing e.g 'export XNAT_ALIAS=your_alias' into the terminal")
    pass