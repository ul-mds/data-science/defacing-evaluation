import os
import argparse
from typing import Tuple

from util import config_vars
from step1_dataimport.step1_a_connection import XNATConnectionProvider
import step1_dataimport.step1_b_dataoverview as stp1_overview
import step1_dataimport.step1_c_filter as stp1_filter
import step1_dataimport.step1_d_import as stp1_import
import step1_dataimport.step1_e_duplicates as stp1_dupl

import step2_defacing.step2 as stp2
import step3_1_get_features.step3 as stp3_1_features
import step3_2_manual_labeling.step3_2_manual_label as stp3_2_label
import step4_modeling.step4_b_model_creation as stp4_model
from util.io import MissingEnvironmentVariables
import step5_stats_visuals.step5 as stp5


# TODO CHECK OB ES INDEX ERROR GIBT TODO CHECK OB ES ASSERTIONERROR GIBT 
# TODO env vars dont stay for some reason
def get_environment_variables() -> Tuple[str, str, str, str]:
    """gets the environment variables needed for logging into the XNAT-Server

    Raises:
        MissingEnvironmentVariables: if the necessary environment variables are not set, this Exception is raised.

    Returns:
        Tuple[str,str,str,str]: returns the environment variable values
    """
    # TODO herausfinden, wieso die environment variablen nicht gesetzt bleiben, evtl. besser sobald man es außerhalb der IDE nutzt? check this.
    # prior step: export these variables with the export VARNAME=varvalue in terminal (works only if you do it in the IDE-Terminal right now- TODO Check this later)
    # set environment variable in ubuntu: export VARNAME=varvalue
    # get environment variable in ubuntu: echo $VARNAME or printenv VARNAME (checking if it worked)
    xnat_alias = os.getenv("XNAT_ALIAS")
    xnat_secret = os.getenv("XNAT_SECRET")
    xnat_addr = os.getenv("XNAT_ADDRESS")
    xnat_project_name = os.getenv("XNAT_PROJECT")
    if not (xnat_alias and xnat_secret and xnat_addr and xnat_project_name):
        raise MissingEnvironmentVariables(
            "There are environment variables missing. Please set them."
        )
    return xnat_alias, xnat_secret, xnat_addr, xnat_project_name


def step1_a_dataimport_get_overview(
    dcm_csv: str = "src/step1_dataimport/dicom_tags_xnat.csv",
    overview_csv: bool | str = False,
) -> str:
    """connects to XNAT with the credentials from the environmentvariables and generates a csv file .
    The csv file contains subject-, session-, and scan-ids for each MRI-scan and their respective properties.
    The properties are given as DICOM-tags through the dcm_csv.

    Args:
        dcm_csv (str, optional): the csv-file that contains all the dicom-tags you want to use for the overview-csv. Defaults to "src/step1_dataimport/dicom_tags_xnat.csv".
        overview_csv (bool|str, optional): path-str of the overview-csv if you interrupted the process and want to continue with your . Defaults to False.

    Returns:
        str: path of the generated output-csv
    """
    # step0 get the csv dataset, dauert laut meiner Berechnung auf meinem arbeits-pc 3,5 h
    xnat_alias, xnat_secret, xnat_addr, xnat_project_name = get_environment_variables()
    xnat_conn = XNATConnectionProvider(xnat_alias, xnat_secret, xnat_addr)
    output_filepath = stp1_overview.DataOverview(
        dcm_csv, xnat_project_name, xnat_conn, overview_csv
    ).run()  # evtl. diese outputfolder übergeben TODO
    return output_filepath


def step1_b_dataimport_filter_and_import(csv_path: str) -> None:
    # TODO disconnect the xnat session? 
    """_summary_

    Args:
        csv_path (str): _description_
    """
    id_tuples = stp1_filter.FilterDataset(csv_path).run()
    xnat_alias, xnat_secret, xnat_addr, xnat_project_name = get_environment_variables()
    xnat_conn = XNATConnectionProvider(xnat_alias, xnat_secret, xnat_addr)
    for subj, sess, scan in id_tuples:
        stp1_import.DataImport(xnat_project_name, xnat_conn).run(subj, sess, scan)


def step1_c_deleteduplicates() -> None:
    # FIXME hier einmal laufen lassen, bedeutet dass ich beim erkennen sie direkt move auch!
    stp1_dupl.IdentifyAndMoveDuplicates().run()

def step2_defacing() -> None:
    """defaces the nifti-files using mri_deface, quickshear, and pydeface."""
    nifti_dir = config_vars.NIFTI_PATH
    for dir in os.listdir(nifti_dir):
        dir_path = os.path.join(nifti_dir, dir)
        for file in os.listdir(dir_path):
            filepath = os.path.join(dir_path, file)
            if os.path.isfile(filepath):
                print("Processing for defacing: ", filepath)
                stp2.Defacing(filepath).quickshear()
                stp2.Defacing(filepath).mri_deface()
                stp2.Defacing(filepath).pydeface()


def argparsing() -> argparse.Namespace:
    """parses the arguments when running the script.

    Returns:
        argparse.Namespace: the chosen options and variables of the user @JOSHUA FIXME
    """
    parser = argparse.ArgumentParser(
        prog="EvaluationModel",
        description='This code was used for the creation of the evaluation-model for the Master Thesis: "Evaluation of Defacing Agorithms"',
        epilog="Thank you! ",
    )

    subparsers = parser.add_subparsers(title="Available Steps", dest="step")

    step1_overview_parser = subparsers.add_parser(
        "step1_overview", help="Execute step 1 - overview"
    )
    step1_overview_parser.add_argument(
        "--csv_dicomtags",
        default="src/step1_dataimport/dicom_tags_xnat.csv",
        help="Path to the Dicom-Tags to extract",
    )
    step1_overview_parser.add_argument(
        "--csv_overviewfile",
        help="If you already started filling this csv file: Please ",
    )

    # step1: import.
    step1_import_parser = subparsers.add_parser(
        "step1_import", help="Execute step 1 import"
    )
    step1_import_parser.add_argument(
        "--overview_csv", help="Path to the overview-csv-file of step1-overview"
    )  # FIXME needs to be mandatory

    # Step 2 and 3
    subparsers.add_parser("step2", help="Execute step 2 defacing")
    subparsers.add_parser("step3_1", help="Execute step 3 feature-extraction")

    # Step4: manual labeling and model creation
    subparsers.add_parser("step3_2", help="for manual labeling")
    subparsers.add_parser("step4_model", help="Generate the machine learning models")

    subparsers.add_parser("step5_visuals", help = "for creating the visuals of the thesis")

    return parser.parse_args()


def main() -> None:
    """controls which steps are executed dependent on the entered args by the user."""
    args = argparsing()

    if args.step == "step1_overview":
        print(f'Data-Import: Get Overview from DICOM-Tags in "{args.csv_dicomtags}". ')
        if args.csv_overviewfile:
            print(f'Continue with "{args.csv_overviewfile}". ')
            # use this with 'output/csv_files/LEUKODYS_2023-09-11.csv' as csv file
            step1_a_dataimport_get_overview(
                dcm_csv=args.csv_dicomtags, overview_csv=args.csv_overviewfile
            )
        else:
            print("Creating new CSV-Overview-File.")
            step1_a_dataimport_get_overview(dcm_csv=args.csv_dicomtags)
    elif args.step == "step1_import":
        print("Data-Import: Filtering and Importing")
        step1_b_dataimport_filter_and_import(args.overview_csv)
        step1_c_deleteduplicates()
    elif args.step == "step2":
        print("Defacing...")
        step2_defacing()
    elif args.step == "step3_1":
        print("Extract and save features")
        stp3_1_features.main()
    elif args.step == "step3_2":
        print("Manual Labeling")
        stp3_2_label.main()
    elif args.step == "step4_model":
        print("Modeling")
        stp4_model.main()
    elif args.step == 'step5_visuals':
        print('Create some Visualisations')
        stp5.main()


def main_for_visualisations_thesis():
    #only used once for some thesis visualisations #TODO can be deleted
    stp3_1_features.main_artefact_threshold_control()
    stp3_1_features.main_visualize_facedetection_for_thesis()
    stp3_1_features.main_visualise_difference_brain_overlap()

if __name__ == "__main__":
    stp5.main()
    main()

