import os
import pandas as pd
import PySimpleGUI as sg
from util import config_vars

# TODO checken ob die excluded versions die so dunkel sind mit anderen einstellungen rendern lassen?

class ManualImageEvaluator: 
    """ creates GUI for manual labeling of the images. 
    """
    def __init__(
        self, image_list:list, orig_image_list:list | None = None
    ) -> None:  # TODO docstring
        self.image_list = image_list
        self.orig_image_list = orig_image_list
        self.window_pos = (100, 100)
        self.keys = ("+", "-","m", "n", "e", "j", "x", "r")
        self.labels = (
            "Defaced (strict)",
            "Defaced (lenient)",
            "Mouth",
            "Nose",
            "Eyes",
            "Jaw",
            "Exclude",
            "Revisit later please",
        )
        self.displaytext = "Tick what you see or enter via keyboard: \n\n+ --- Defaced properly (strict))\n- --- Defaced properly (lenient)\nm --- Mouth\nn --- Nose\ne --- Eyes\nj --- Jawline/Facestructure\nx --- Exclude\nr --- Revisit later.\nKlick Back to redo the last picture. Klick Close or X to discontinue. Klick Complete or hit Enter to commit the change."
        self.checkboxes = [
            sg.Checkbox(value, key=k) for value, k in zip(self.labels, self.keys)
        ]
        self.current_img_pos = 0
        self.orig = False
        if orig_image_list is not None:
            for idx, image in enumerate(orig_image_list):
                if os.path.isfile(image):
                    self.current_img_pos = idx
                    self.orig = True
                    break

    def __set_gui_layout(self):
        """creates a GUI for manual evaluation of facial feature occurence in imagefiles

        Args:
            imagepath (str): Path to the imagefile

        Returns:
            dict or None: A dictionary containing info about whether facial features were identified as a bool. Returns None if the window is closed or Back Button is clicked
        """
        sg.set_options(font=("Times", 12, "bold"))
        sg.theme("DefaultNoMoreNagging")
        print(self.checkboxes)
        
        if self.orig: # hier check einbauen, falls das original image nicht verfügbar ist. 
            image_layout = [
                [
                    sg.Image(filename = self.image_list[self.current_img_pos], key='-IMG-'),
                    sg.Image(filename = self.orig_image_list[self.current_img_pos], key='-IMG-ORG-' ),
                ],
                [
                    sg.Text(f"Defaced: {self.image_list[self.current_img_pos]}"),
                    sg.Text(f"Original: {self.orig_image_list[self.current_img_pos]}"),
                ],
            ]
        else:
            image_layout = [
                [sg.Image(filename = self.image_list[self.current_img_pos], key = '-IMG-')],
                [sg.Text(f"Imagepath: {self.image_list[self.current_img_pos]}")],
            ]
        self.layout = [
            image_layout,
            [sg.Text(self.displaytext)],
            [sg.Input(key="-IN-", enable_events=True), sg.Input(key='-COMM-', default_text = "Comment",enable_events = True)],
            self.checkboxes,
            [
                sg.Button(
                    '<', key = '-PREV-', bind_return_key=False, button_color= 'grey'
                ),
                sg.Button(
                    "Complete",
                    key="-COMPLETE-",
                    bind_return_key=True,
                    button_color="green",
                ),
                sg.Button(
                    "Back", key="-BACK-", bind_return_key=False, button_color="red"
                ),
                sg.Button(
                    "Close", key="-CLOSE-", bind_return_key=False, button_color="black"
                ),
                sg.Button(
                    '>', key = '-NEXT-', bind_return_key=False, button_color= 'grey'
                )
            ],
            
        ]

    def __run_gui_functionality(self):  # TODO docstring erstellen.
        window = sg.Window(
            self.image_list[0],
            self.layout,
            element_justification="c",
            location=self.window_pos,
            resizable=True,
            no_titlebar=False,
            size = (600,600),
            finalize=True
        )  # location decides where in the screen the window pops up
        window.maximize()
        window.bind('<Right>', '-NEXT-')
        window.bind('<Left>', '-PREV-')

        #current_image_index = 0
        while True:
            event, values = window.read()
            self.window_pos = (
                window.current_location()
            )  # TODO anpassen von der neuen Position des Fensters automatisch, dafür muss window_pos zurückgeführt werden aber evtl. auch lassen...
            if event == sg.WIN_CLOSED or event == "-CLOSE-":
                window.close()
                return None  # indicates discontinuation

            if event == "-COMPLETE-" or event == " ":
                checkbox_values = {
                    key: values[self.keys[i]] for i, key in enumerate(self.labels)
                }
                if values["-COMM-"] != 'Comment':
                    checkbox_values['Comment'] = values['-COMM-'] 
                else: 
                    checkbox_values['Comment'] = ""
                window.close()
                return checkbox_values

            if event == "-BACK-":
                window.close()
                return "BACK"
            
            if event == '-PREV-':
                self.current_img_pos = (self.current_img_pos-1) % len(self.image_list)
                window['-IMG-'].update(filename = self.image_list[self.current_img_pos])
                if self.orig and os.path.isfile(self.orig_image_list[self.current_img_pos]):
                    window['-IMG-ORG-'].update(filename = self.orig_image_list[self.current_img_pos])

            if event == '-NEXT-':
                self.current_img_pos = (self.current_img_pos+1) % len(self.image_list)
                window['-IMG-'].update(filename = self.image_list[self.current_img_pos])
                if self.orig and os.path.isfile(self.orig_image_list[self.current_img_pos]):
                    window['-IMG-ORG-'].update(filename = self.orig_image_list[self.current_img_pos])
            if event == "-COMM-":
                pass
            if event == "-IN-":
                entered_text = values["-IN-"].lower()
                window["-IN-"].update("")
                try:
                    for key in entered_text:
                        checkbox_value = values[key]
                        if (
                            checkbox_value
                        ):  # with each enter of key, it will change the Checkbox-Tick
                            window[key].update(False)
                        elif not checkbox_value:
                            window[key].update(True)
                except KeyError:
                    pass

    def run(self) -> dict|None:  # TODO docstring erstellen
        """_summary_

        Returns:
            _type_: _description_
        """
        self.__set_gui_layout()
        feedback = self.__run_gui_functionality()
        return feedback


def make_manual_label_csv(csv_name):
    """Iterates through Imagefiles in a folder, performs manual_facialfeature_eval per Image and saves the checkbox values in a CSV File.

    Args:
        image_folder (str): Path to the folder containing the image files
        csv_name (str): Name of the CSV file to save the evaluation outcome in
    """
    user_input_dict = {}
    image_folder = config_vars.IMAGES_PATH
    #output_df = pd.DataFrame() #TODO DELETE?! ODER WAS?
    for root, _, files in os.walk(image_folder):
        try:  # for initialisation and continuation of a csv-file
            fileframe = pd.read_csv(csv_name)
        except FileNotFoundError as e: #TODO specific exception reintun!
            print('\x1b[0;30;43m',e,'\x1b[0m')# gelb
            fileframe = pd.DataFrame()
        if 'original' in root:#überspringe die originalen
            continue
        try:
            files_realpaths = [os.path.join(root,file) for file in files]
            files_originals_list = [file.replace('_pydeface', '').replace('_quickshear', '').replace('_mrideface', '').replace('pydeface', 'original').replace('mrideface', 'original').replace('quickshear', 'original') for file in files_realpaths]
            filename_for_csv = files_originals_list[0]
            new_root = (root.replace('image_files_2d', 'niftis'))
            new_filename = os.path.join(new_root, os.listdir(new_root)[0]).replace('.nii.log', '.nii.gz') #ÄNDERUNG NOCH NICHT AUSPROBIERT: TESTEN! TODO

            try:  # skips what has already been done
                if new_filename in fileframe["Filepath"].values:
                    continue
            except Exception as e:  # TODO besser genaue exception wählen, damit unerwartete exceptions abgefangen werden?
                print('\x1b[6;30;42m',e,'\x1b[0m') #grün
                pass
            try:  # TODO hier die headers setzen je nach option???
                headers = [
                    "Filepath",
                    "Filepath Image",
                    "Defaced (strict)",
                    "Defaced (lenient)",
                    "Mouth",
                    "Nose",
                    "Eyes",
                    "Jaw/Facestructure",
                    "Exclude",
                    "Revisit", 
                    "Comments"
                ]
                checkbox_values = ManualImageEvaluator(files_realpaths,files_originals_list
                    ).run()
                    
                if (
                    checkbox_values is None
                ):  # controls discontinuation of evaluation-process
                    break
                if checkbox_values == "BACK" and len(fileframe.index) == 0:
                    raise Exception(
                        "You want to go further back than possible. Please restart!"
                    )
                elif (
                    checkbox_values == "BACK" and len(fileframe.index) > 0
                ):  # controls back-function: correcting mistakes on last submitted entry
                    fileframe = fileframe[:-1]
                    fileframe.to_csv(csv_name, index=False)
                    make_manual_label_csv(csv_name=csv_name)
                    checkbox_values = None  # important for discontinuation of the last make_benchmark_csv process, in favor of the new one
                    break
                else:  # update the csv-file
                    user_input_dict["Filepath"] = new_filename
                    user_input_dict["Filepath Image"] = filename_for_csv
                    user_input_dict.update(checkbox_values)
                    fileframe = pd.DataFrame(user_input_dict, index=[0])
                    if not os.path.isfile(csv_name):
                        fileframe.to_csv(csv_name, mode="a", index=False, header=headers)
                    else:
                        fileframe.reset_index(drop=True, inplace=True)
                        fileframe.to_csv(csv_name, mode="a", index=False, header=False)
                try:
                    if (
                        checkbox_values is None
                    ):  # controls discontinuation of evaluation-process
                        break
                except:  # TODO maybe here specific Exception?
                    continue
            except Exception as e:
                print("An Exception occured: ", e)

        except IndexError: #TODO check again if avoidable
            pass
            #print('No files on this level of for loop.')
        
def main(): 
    make_manual_label_csv(os.path.join(config_vars.CSV_PATH_STEP3, "manual_evaluation_FINALVERSION.csv"))

if __name__ == "__main__":
    main()
