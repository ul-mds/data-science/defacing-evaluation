import os
import subprocess
from util.io import make_dir_if_not_exists
from util import config_vars

class Defacing:
    """ executes the defacing.
    """
    # TODO frage klären, wie das mit fsl dann funktioniert.. also mit bet als brain extraction method. 
    def __init__(self, file): 
        self.file = file 
        self.basename = os.path.basename(self.file)
        self._file_ext = '.nii.gz' 
        self.basename_no_ext = self.basename.replace(self._file_ext, '')
        self.dir = os.path.dirname(self.file)
        
    def run_terminal_command(self, command:list): # TODO evtl. hier mal loggen, welche Ergebnisse das deface hat, ob es klappt oder eben auch nicht.. auch chekcen wie viele voxel entfernt wurden laut pydeface
        """runs the given terminal command 

        Args:
            command (list): a defacing command for the subprocess to run
        """
        try:
            result = subprocess.run(command)
            print(result, result.stderr, result.stdout) #das hier evtl. irgendwann nicht mehr printen sondern nur loggen TODO
        except subprocess.CalledProcessError as e:
            print("\x1b[0;31m", "Error: ", "\x1b[0m", e)

    def quickshear(self):
        """defines and runs the brainmask and quickshear commands in terminal as subprocess. Skips existing files. 
        """
        filename_brainmask = f"{os.path.join(self.dir, 'brainmask', self.basename_no_ext)}_brainmask.nii.gz"
        filename_final = f"{os.path.join(self.dir, 'quickshear', self.basename_no_ext)}_quickshear.nii.gz"
        make_dir_if_not_exists(os.path.dirname(filename_brainmask))
        make_dir_if_not_exists(os.path.dirname(filename_final))
        brainmask_process = [
            "bet",
            self.file,
            filename_brainmask,
        ]
        command_quickshear = [
            "python3",
            config_vars.QS_SCRIPT,
            self.file,
            filename_brainmask,
            filename_final,
        ]
        if not os.path.exists(filename_final):
            self.run_terminal_command(brainmask_process)
            self.run_terminal_command(command_quickshear)  
        else:
            print('Skip existing file: ', filename_final)

    def mri_deface(
        self
    ):  
        """ defines and runs the mri_deface command in terminal as subprocess. Moves the generated nifti-log file to the correct subdirectory. Skips existing files.
        """
        filename_final = f"{os.path.join(self.dir, 'mrideface', self.basename_no_ext)}_mrideface.nii.gz"
        make_dir_if_not_exists(os.path.dirname(filename_final))
        command_mrideface = [
            config_vars.MRI_DEF_SCRIPT,
            self.file,
            config_vars.MRI_DEF_TALAIRACH,
            config_vars.MRI_DEF_FACE,
            filename_final,
        ]
        if not os.path.exists(filename_final):
            self.run_terminal_command(command_mrideface)
            niilog_filepath = f"{(os.path.join(os.getcwd(), self.basename_no_ext))}_mrideface.nii.log" #TODO CHECK IF THIS IS STILL WORKING AND BULLETPROOF?
            mridef_dir = os.path.join(self.dir, 'mrideface')
            self.run_terminal_command(['mv', niilog_filepath, mridef_dir])
        else:
            print('Skip existing file: ', filename_final)

    def pydeface(self):
        """defines and runs the pydeface command in terminal as subprocess. Skips existing files. 
        """
        filename_final = f"{os.path.join(self.dir, 'pydeface', self.basename_no_ext)}_pydeface.nii.gz"
        make_dir_if_not_exists(os.path.dirname(filename_final))
        command_pydeface = [
            "pydeface",
            "--outfile",
            filename_final,
            self.file,
        ]
        if not os.path.exists(filename_final):
            self.run_terminal_command(command_pydeface)
        else:
            print(
                "Skip existing file: ", filename_final
            )
            pass