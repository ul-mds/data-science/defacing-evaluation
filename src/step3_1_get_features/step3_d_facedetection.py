from mtcnn import MTCNN
import cv2
import face_recognition as fr
from typing import Tuple
from shapely.geometry import Polygon

#TODO REFACTORING: FUNCTION OUT OF IF ELSE MACHEN

class FaceDetection:
    def __init__(self, imagepath) -> None:
        self.imagepath = imagepath

    def mtcnn_detect(self) -> float:
        img = cv2.cvtColor(cv2.imread(self.imagepath), cv2.COLOR_BGR2RGB)
        detector = MTCNN()
        faces = detector.detect_faces(img)
        print(f"Nr of faces found: {len(faces)}")
        # manually setting a rectangle where the faces always are
        # self.rect_shape_fix, self.area_fixed_rect = self.get_fixed_rectangle(img)
        if len(faces) > 0:
            self.rect_shape_fix, self.area_fixed_rect = self.get_fixed_rectangle(img)
            list_of_ious = []
            for face in faces:
                x, y, w, h = face["box"]
                iou = self.__get_iou(x, y, w, h)
                list_of_ious.append(iou)
                cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

            iou = max(list_of_ious)
            print(iou, list_of_ious)
            #cv2.imwrite(f"/home/mds-user/Documents/VISUALISATIONS/FaceDetection/facedetection_mtcnn_example_4.jpg", cv2.cvtColor(img, cv2.COLOR_RGB2BGR))

        else:
            iou = 0.0
        del img
        return iou

    def face_recognition_detect(self) -> float:
        img = fr.load_image_file(self.imagepath)
        face_locations = fr.face_locations(img)
        print(f"Nr of faces found: {len(face_locations)}")
        if len(face_locations) > 0:
            self.rect_shape_fix, self.area_fixed_rect = self.get_fixed_rectangle(img)
            list_of_ious = []
            for top, right, bottom, left in face_locations: 
                #cv2.rectangle(img, (left,top), (right,bottom), (0,255,0),2)
                iou_temp = self.__get_iou(left, top, right - left, bottom - top)
                list_of_ious.append(iou_temp)
                cv2.rectangle(img, (left, top), (right, bottom), (0, 255, 0), 2)

            iou = max(list_of_ious)
            print(iou, list_of_ious)
            #cv2.imwrite("/home/mds-user/Documents/VISUALISATIONS/FaceDetection/facedetection_facerec_example_4.jpg", cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
        else:
            iou = 0.0
        del img
        return iou

    def opencv(
        self, haarcascade="haarcascade_frontalface_alt2.xml", minNeigh=4
    ) -> float:
        face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + haarcascade)
        img = cv2.imread(self.imagepath)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(
            gray, 1.1, minNeigh, flags=cv2.CASCADE_SCALE_IMAGE
        )
        if len(faces) > 0:
            self.rect_shape_fix, self.area_fixed_rect = self.get_fixed_rectangle(img)
            list_of_ious = []
            for x, y, w, h in faces:
                iou = self.__get_iou(x, y, w, h)
                list_of_ious.append(iou)
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

            iou = max(list_of_ious)
            print(iou, list_of_ious)
            #cv2.imwrite("/home/mds-user/Documents/VISUALISATIONS/FaceDetection/facedetection_opencv_example_4.jpg", cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
        else:
            iou = 0.0
        print(f"Nr of faces found: {len(faces)}")
        del img
        return iou

    def get_fixed_rectangle(self, img) -> tuple[Polygon, int]:  # evtl. self unnötig
        height, width, _ = img.shape  # prüfen ob die shape sich ändert?
        center = (width / 2, height / 2)
        rect_width = (width / 4) * 2.5
        rect_height = (height / 6) * 2.5
        # coordinates
        # top left corner, bottom right corner
        x_left = int(center[0] - 0.5 * rect_width)
        y_top = int(center[1] - 0.5 * rect_height + 70)
        x_right = int(center[0] + 0.5 * rect_width)
        y_bottom = int(center[1] + 0.5 * rect_height + 70)

        top_left = (x_left, y_top)
        top_right = (x_right, y_top)
        bottom_left = (x_left, y_bottom)
        bottom_right = (x_right, y_bottom)

        area_fixed_rect = abs(x_left - x_right) * abs(y_top - y_bottom)  # area pixels
        cv2.rectangle(
            img, top_left, bottom_right, (0, 0, 255), 2
        )  # for display in the facedetection (white one is fixed one)
        rect_fixed_shape = Polygon([top_left, top_right, bottom_right, bottom_left])
        return rect_fixed_shape, area_fixed_rect

    def __get_iou(self, x, y, w, h) -> float:
        area_fd_rect = abs(x - (x + w)) * abs(y - (y + h))
        rect_shape_of_faces = Polygon([(x, y), (x + w, y), (x + w, y + h), (x, y + h)])
        intersection_area = self.rect_shape_fix.intersection(
            rect_shape_of_faces
        ).area  # area of intersection
        iou = intersection_area / (
            area_fd_rect + self.area_fixed_rect - intersection_area
        )
        return iou

    def run_all_and_max(self) -> Tuple[float, float, float]:
        print("detecting faces ...")
        print("MTCNN")
        iou_mtcnn = self.mtcnn_detect()
        print("FACEREC")
        iou_facerec = self.face_recognition_detect()
        print("OPENCV")
        iou_opencv = self.opencv()
        return (
            iou_facerec,
            iou_mtcnn,
            iou_opencv,
        )