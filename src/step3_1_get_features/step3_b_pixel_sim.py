from PIL import Image
from image_similarity_measures.evaluate import evaluation
from typing import Tuple


class ImageComparison2D:
    """Compares image-related similarity metrics and the amount of different pixels"""

    def __init__(self, imagepath_original: str, imagepath_defaced: str) -> None:
        self._imagepath_original = imagepath_original
        self._imagepath_defaced = imagepath_defaced

    def __compare_amount_of_pixels(self) -> float:
        """counts the amount of pixels that differ between the two images.

        Returns:
            int: amount of different pixels of the two images
        """
        img1 = Image.open(self._imagepath_original)
        img2 = Image.open(self._imagepath_defaced)
        diff_count: float = 0.0
        width, height = img1.size
        print('IMG RESOLUTION', width, height)
        for y in range(height):
            for x in range(width):
                pixel1 = img1.getpixel((x, y))
                pixel2 = img2.getpixel((x, y))

                if pixel1 != pixel2:
                    diff_count += 1
        img1.close()
        img2.close()
        del img1, img2
        return (diff_count / (width*height))

    def __get_similarity_scores(self) -> dict:
        """gets similarity scores for the images.

        Returns:
            dict: the similarity scores.
        """
        metrics = ["rmse", "psnr", "ssim", "fsim"]
        sim_scores = evaluation(
            self._imagepath_original, self._imagepath_defaced, metrics=metrics
        )
        return sim_scores

    def run(self) -> Tuple[float, dict]:
        pixel_diff_normed = self.__compare_amount_of_pixels()
        sim_scores = self.__get_similarity_scores()
        return pixel_diff_normed, sim_scores
