import os
import pandas as pd

from util import config_vars
from step3_1_get_features.step3_b_pixel_sim import ImageComparison2D
from step3_1_get_features.step3_a_3drender import Make3DRender
import step3_1_get_features.step3_d_facedetection as fd
from step3_1_get_features.step3_c_voxel_analysis import VoxelAnalysis

# TODO csv's parametrisieren dass überall dann auf gleiches zurückgegriffen wird garantiert. 

def main():
    csv_file_path = f"{config_vars.CSV_PATH_STEP3}/feature_collection_results.csv"
    csv_headers = [
        "Original Nifti",
        "Defaced Nifti",
        "Frontal Orig Img",
        "Frontal Def Img",
        "Pixel Diff",
        "rmse",
        "psnr",
        "ssim",
        "fsim",
        "IOU OpenCV",
        "IOU MTCNN",
        "IOU FaceRec",
        "Nifti-Dimensions",
        "Resolution",
        "Total Nr Brain Voxels",
        "Brain relative to head",
        "Percentage of Brain altered",
        "Nr of altered Brain Voxels",
        "Nr of altered Voxels through defacing (thresh)",
        "Percentage of Head Voxels altered through defacing (thresh)",
        "Percentage of Head Voxels completely removed through defacing (thresh)",
        "Dask Array used"
    ]
    try:
        csv_df = pd.read_csv(csv_file_path)
    except FileNotFoundError:
        csv_df = pd.DataFrame

    for root, _, files in os.walk(config_vars.NIFTI_PATH):
        if len(files) == 0:
            continue
        for file in files:
            if not any(
                string in file
                for string in ["brainmask", "mrideface", "pydeface", "quickshear"]
            ):
                original_nifti_path = os.path.join(root, file)
                print("ORIGINAL: ", original_nifti_path)
                frontal_orig_img_path = Make3DRender(original_nifti_path).render()
                print("FRONTAL_ORIG: ", frontal_orig_img_path)
            if any(
                string in file for string in ["mrideface", "quickshear", "pydeface"]
            ) and file.endswith(".nii.gz"):
                defaced_nifti_path = os.path.join(root, file)
                print("DEFACED:", defaced_nifti_path)
                try:  # skips what has already been done
                    if defaced_nifti_path in csv_df["Defaced Nifti"].values:
                        continue
                except (
                    Exception
                ) as e:  # TODO besser genaue exception wählen, damit unerwartete exceptions abgefangen werden
                    print(e)
                frontal_def_img_path = Make3DRender(defaced_nifti_path).render()
                print("FRONTAL_DEF: ", frontal_def_img_path)
                if frontal_def_img_path == 'EOFError' or frontal_orig_img_path == 'EOFError':
                    current_data = [[original_nifti_path, defaced_nifti_path, frontal_orig_img_path, frontal_def_img_path, 'ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR','ERROR']]
                else:
                    pixel_diff_normed,sim_scores = ImageComparison2D(
                        frontal_orig_img_path, frontal_def_img_path
                    ).run()
                    (
                        iou_facerec,
                        iou_mtcnn,
                        iou_opencv,
                    ) = fd.FaceDetection(frontal_def_img_path).run_all_and_max()
                    brainmask_path = (
                        defaced_nifti_path.replace("mrideface", "brainmask")
                        .replace("quickshear", "brainmask")
                        .replace("pydeface", "brainmask")
                    )
                    feature_list = VoxelAnalysis(
                        original_nifti_path, defaced_nifti_path, brainmask_path
                    ).run()

                    current_data = [
                        [
                            original_nifti_path,
                            defaced_nifti_path,
                            frontal_orig_img_path,
                            frontal_def_img_path,
                            pixel_diff_normed,
                            sim_scores["rmse"],
                            sim_scores["psnr"],
                            sim_scores["ssim"],
                            sim_scores["fsim"],
                            iou_opencv,
                            iou_mtcnn,
                            iou_facerec,
                        ]
                        + feature_list
                    ]
                df = pd.DataFrame(current_data, columns=csv_headers)
                df.to_csv(
                    csv_file_path,
                    mode="a",
                    header=not os.path.exists(csv_file_path),
                    index=False,
                )


def main_artefact_threshold_control():
    VoxelAnalysis('output/niftis/XNAT_MF_S00014_XNAT_MF_E00016_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00014_XNAT_MF_E00016_301000_NIFTI/mrideface/301000_3d_flair_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00014_XNAT_MF_E00016_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination() 
    VoxelAnalysis('output/niftis/XNAT_MF_S00017_XNAT_MF_E00020_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00017_XNAT_MF_E00020_301000_NIFTI/pydeface/301000_3d_flair_pydeface.nii.gz', 'output/niftis/XNAT_MF_S00017_XNAT_MF_E00020_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/quickshear/301000_3d_flair_quickshear.nii.gz', 'output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/quickshear/301000_3d_flair_quickshear.nii.gz', 'output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/quickshear/301000_3d_flair_quickshear.nii.gz', 'output/niftis/XNAT_MF_S00022_XNAT_MF_E00598_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00035_XNAT_MF_E00080_701000_NIFTI/701000_t1_mpr_rage_sag_km.nii.gz', 'output/niftis/XNAT_MF_S00035_XNAT_MF_E00080_701000_NIFTI/pydeface/701000_t1_mpr_rage_sag_km_pydeface.nii.gz', 'output/niftis/XNAT_MF_S00035_XNAT_MF_E00080_701000_NIFTI/brainmask/701000_t1_mpr_rage_sag_km_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00037_XNAT_MF_E00094_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00037_XNAT_MF_E00094_301000_NIFTI/mrideface/301000_3d_flair_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00037_XNAT_MF_E00094_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00038_XNAT_MF_E00108_201000_NIFTI/201000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00038_XNAT_MF_E00108_201000_NIFTI/quickshear/201000_3d_flair_quickshear.nii.gz', 'output/niftis/XNAT_MF_S00038_XNAT_MF_E00108_201000_NIFTI/brainmask/201000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00042_XNAT_MF_E00118_601000_NIFTI/601000_t1_mpr_rage_sag_km.nii.gz', 'output/niftis/XNAT_MF_S00042_XNAT_MF_E00118_601000_NIFTI/pydeface/601000_t1_mpr_rage_sag_km_pydeface.nii.gz', 'output/niftis/XNAT_MF_S00042_XNAT_MF_E00118_601000_NIFTI/brainmask/601000_t1_mpr_rage_sag_km_brainmask.nii.gz').run_artefact_threshold_examination()
    VoxelAnalysis('output/niftis/XNAT_MF_S00162_XNAT_MF_E00473_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00162_XNAT_MF_E00473_301000_NIFTI/pydeface/301000_3d_flair_pydeface.nii.gz', 'output/niftis/XNAT_MF_S00162_XNAT_MF_E00473_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run_artefact_threshold_examination()

def main_visualize_facedetection_for_thesis():
    fd.FaceDetection('output/image_files_2d/XNAT_MF_S00063_XNAT_MF_E00164_601000_NIFTI/original/601000_t1_mpr_rage_sag_km_front.png').run_all_and_max()
    fd.FaceDetection('output/image_files_2d/XNAT_MF_S00175_XNAT_MF_E00505_601000_NIFTI/pydeface/601000_t1_mpr_rage_sag_km_pydeface_front.png').run_all_and_max()
    fd.FaceDetection('output/image_files_2d/XNAT_MF_S00263_XNAT_MF_E00944_801000_NIFTI/quickshear/801000_3d_brainview_t1w_quickshear_front.png').run_all_and_max()
    fd.FaceDetection('output/image_files_2d/XNAT_MF_S00165_XNAT_MF_E00480_301000_NIFTI/quickshear/301000_3d_flair_quickshear_front.png').run_all_and_max()
    fd.FaceDetection('output/image_files_2d/XNAT_MF_S00038_XNAT_MF_E00110_301000_NIFTI/quickshear/301000_3d_flair_quickshear_front.png').run_all_and_max()

def main_visualise_difference_brain_overlap():
    VoxelAnalysis('output/niftis/XNAT_MF_S00071_XNAT_MF_E00215_201001_NIFTI/201001_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00071_XNAT_MF_E00215_201001_NIFTI/mrideface/201001_3d_flair_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00071_XNAT_MF_E00215_201001_NIFTI/brainmask/201001_3d_flair_brainmask.nii.gz').run()
    VoxelAnalysis('output/niftis/XNAT_MF_S00223_XNAT_MF_E00783_901_NIFTI/901_st1w_3d_iso_sag_km.nii.gz', 'output/niftis/XNAT_MF_S00223_XNAT_MF_E00783_901_NIFTI/mrideface/901_st1w_3d_iso_sag_km_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00223_XNAT_MF_E00783_901_NIFTI/brainmask/901_st1w_3d_iso_sag_km_brainmask.nii.gz').run()
    VoxelAnalysis('output/niftis/XNAT_MF_S00202_XNAT_MF_E00695_301001_NIFTI/301001_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00202_XNAT_MF_E00695_301001_NIFTI/mrideface/301001_3d_flair_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00202_XNAT_MF_E00695_301001_NIFTI/brainmask/301001_3d_flair_brainmask.nii.gz').run()
    VoxelAnalysis('output/niftis/XNAT_MF_S00042_XNAT_MF_E00118_601000_NIFTI/601000_t1_mpr_rage_sag_km.nii.gz', 'output/niftis/XNAT_MF_S00042_XNAT_MF_E00118_601000_NIFTI/mrideface/601000_t1_mpr_rage_sag_km_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00042_XNAT_MF_E00118_601000_NIFTI/brainmask/601000_t1_mpr_rage_sag_km_brainmask.nii.gz').run()
    VoxelAnalysis('output/niftis/XNAT_MF_S00141_XNAT_MF_E00410_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00141_XNAT_MF_E00410_301000_NIFTI/mrideface/301000_3d_flair_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00141_XNAT_MF_E00410_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run()
    VoxelAnalysis('output/niftis/XNAT_MF_S00141_XNAT_MF_E00412_301000_NIFTI/301000_3d_flair.nii.gz', 'output/niftis/XNAT_MF_S00141_XNAT_MF_E00412_301000_NIFTI/mrideface/301000_3d_flair_mrideface.nii.gz', 'output/niftis/XNAT_MF_S00141_XNAT_MF_E00412_301000_NIFTI/brainmask/301000_3d_flair_brainmask.nii.gz').run()

