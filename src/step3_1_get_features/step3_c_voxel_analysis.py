import numpy as np
import nibabel as nib
from util import config_vars
import dask.array as da
import os
import matplotlib.pyplot as plt
import seaborn as sns
from util.io import make_dir_if_not_exists

# NOTE due to the normalization, when creating nuances (non binary) headmasks the values need to be multiplied with a big number again. or with the maximum value that was used for the normalisation in the first place
class VoxelAnalysis:
    def __init__(self, orig_nifti, def_nifti, brainmask_nifti) -> None:
        self.orig_nifti = orig_nifti
        self.def_nifti = def_nifti
        self.brainmask_nifti = brainmask_nifti

    # das hier evtl. nicht beides in einem schritt machen..
    def __load_image(self):
        print("loading image...")
        self.dask_arr = False
        # if Size of Nifti bigger than 70 MB use dask, else use numpy array
        if (os.path.getsize(self.def_nifti) / 1000000) > 70:
            print("Very large file, used dask array")
            self.dask_arr = True
            img_def = nib.load(self.def_nifti)
            img_fdata_def = da.from_array(img_def.get_fdata(), chunks=(100, 100, 100))
            del img_def
            img_orig = nib.load(self.orig_nifti)
            img_fdata_orig = da.from_array(img_orig.get_fdata(), chunks=(100, 100, 100))
            del img_orig
        else:
            print("File size <= 70MB, default load")
            img_def = nib.load(self.def_nifti)
            img_fdata_def = img_def.get_fdata()
            del img_def
            img_orig = nib.load(self.orig_nifti)
            img_fdata_orig = img_orig.get_fdata()
            del img_orig
        return img_fdata_def, img_fdata_orig

    def __get_resolution_and_dim(self, fdata):
        dimensions = fdata.shape
        resolution = dimensions[0] * dimensions[1] * dimensions[2]
        return dimensions, resolution

    def __check_normalisation(self, data: nib.load):
        if (np.min(data) != 0) or (np.max(data) != 1):
            raise Exception(
                "The normalisation was not successful. Further processing is omitted. "
            )

    def __create_nifti_headmask_for_manual_control(self, headmask, naming):
        print("creating headmask...")
        outputname = f'{self.orig_nifti.replace(".nii.gz", "")}_{naming}.nii.gz'.replace(
            "niftis", "NiftiExaminations"
        )  # hier ist die endung noch dran #FIXME
        print(outputname)
        make_dir_if_not_exists(os.path.dirname(outputname))
        img_for_affine = nib.load(self.orig_nifti)
        niftiimg = nib.Nifti1Image(
            headmask.astype(np.uint8), img_for_affine.affine
        )  # affine = np.eye(4)) #TODO check, which one is better - np.eye or img_for_affine.affine!#FIXME
        nib.save(niftiimg, outputname)

    def __load_and_normalize(self):
        """for comparable threshold for artefact removal the data needs to be unified by normalizing it

        Raises:
            ValueError: in case there is a mismatch of dimensions

        Returns:
            _type_: _description_
        """
        print("normalizing...")
        img_data_def, img_data_orig = self.__load_image()
        if img_data_def.shape != img_data_orig.shape:
            raise ValueError(
                "The Images do not have the same shape. Is there a mismatch?"
            )
        max_def = np.max(img_data_def)
        max_orig = np.max(img_data_orig)
        normalized_def_fdata = img_data_def / max_def
        normalized_orig_fdata = img_data_orig / max_orig
        self.__check_normalisation(normalized_def_fdata)
        self.__check_normalisation(normalized_orig_fdata)
        return normalized_def_fdata, normalized_orig_fdata

    def __get_brainmask_difference_overlap(self, brainmask, difference):
        """creates True-False masks of the brain and voxels altered through defacing and counts the overlapping voxels.


        Args:
            brainmask (np.array): _description_
            difference (np.array): _description_

        Returns:
            _type_: _description_
        """
        # evtl. die amount of removed brainvoxels auch zählen?!
         # True False maske
        difference_nonzero = difference > 0
        if self.dask_arr:
            brainmask_dask = da.from_array(brainmask, chunks=difference.chunks)
            brainmask_nonzero = brainmask_dask > 0 
            overlap = da.logical_and(brainmask_nonzero, difference_nonzero)
            # Count the number of overlapping voxels
            del difference_nonzero
            altered_brain_voxels_absolute = da.sum(overlap).compute()
            del overlap
        else:
            brainmask_nonzero = brainmask > 0 
            overlap = brainmask_nonzero & difference_nonzero
            #self.__create_nifti_headmask_for_manual_control(overlap, 'OVERLAP_BRAIN_EVAL') # for control nifti: uncomment
            del difference_nonzero
            indices_overlap = np.argwhere(overlap)
            del overlap
            altered_brain_voxels_absolute = indices_overlap.shape[0]
            #self.__create_nifti_headmask_for_manual_control(brainmask_nonzero, 'BRAINMASK_NOZERO') # for control nifti: uncomment

        totalbrainvoxels = np.count_nonzero(brainmask)
        percentage_of_brain_altered = altered_brain_voxels_absolute / totalbrainvoxels
        return (
            altered_brain_voxels_absolute,
            percentage_of_brain_altered,
            totalbrainvoxels,
        )

    def __get_altered_brainvoxels_percentage(self, img_data_def, img_data_orig):
        altered_voxels = np.abs(img_data_orig - img_data_def)
        # deal with brainmask
        image_brainmask = nib.load(self.brainmask_nifti)
        image_brainmask_data = image_brainmask.get_fdata()
        del image_brainmask
        (
            altered_brain_voxels,
            percentage_of_brain_altered,
            totalbrainvoxels,
        ) = self.__get_brainmask_difference_overlap(
            image_brainmask_data, altered_voxels
        )
        return percentage_of_brain_altered, altered_brain_voxels, totalbrainvoxels

    def __get_thresholded_preliminaries(self, img_data_def, img_data_orig):

        threshold_mask = img_data_orig <= 0.03
        if self.dask_arr:
            img_data_def = da.where(threshold_mask, 0.0, img_data_def)
            img_data_orig = da.where(threshold_mask, 0.0, img_data_orig)
            del threshold_mask
            headmask_count = da.count_nonzero(img_data_orig).compute()  # Compute to get the actual result
        else:
            img_data_def[threshold_mask] = 0.0
            img_data_orig[threshold_mask] = 0.0
            del threshold_mask
            headmask_count = np.count_nonzero(img_data_orig)
        return img_data_def, img_data_orig, headmask_count

    def __get_altered_voxels_percentage(
        self, img_data_def, img_data_orig, headmask_count
    ):
        if self.dask_arr:
            altered_voxels = da.abs(img_data_orig - img_data_def)
            # Count non-zero elements
            altered_voxels_count = da.count_nonzero(altered_voxels).compute()  # Compute to get the actual result
            # Calculate the percentage
            del altered_voxels
        else:
            altered_voxels = np.abs(img_data_orig - img_data_def)
            altered_voxels_count = np.count_nonzero(altered_voxels)
            #self.__create_nifti_headmask_for_manual_control(altered_voxels>0, 'DIFFERENCE_MASK') #uncomment fir control nifti
            del altered_voxels
        percentage_headvoxels_altered_thresh_corrected = (
            altered_voxels_count / headmask_count
        )
        return percentage_headvoxels_altered_thresh_corrected, altered_voxels_count

    def __get_amount_of_completely_removed_voxels(
        self, img_data_def, img_data_orig, headmask_count
    ):  # thresholded
        if self.dask_arr:
            zero_count_def = da.sum(img_data_def == 0).compute()  # Compute to get the actual result
            zero_count_orig = da.sum(img_data_orig == 0).compute()  # Compute to get the actual result
        else:
            zero_count_def = np.sum(img_data_def == 0)
            zero_count_orig = np.sum(img_data_orig == 0)

        difference_in_zeros = zero_count_def - zero_count_orig
        # das dann nochmal an head normalisieren
        percentage_of_completely_removed_voxels = difference_in_zeros / headmask_count
        return percentage_of_completely_removed_voxels

    def run(self):
        #load and normalize
        (
            img_data_def,
            img_data_orig,
        ) = self.__load_and_normalize()  # original no threshold applied
        dim, res = self.__get_resolution_and_dim(
            img_data_def
        )  # just for checking dimensions later metadata
        (
            percentage_of_brain_altered,
            altered_brain_voxels,
            totalbrainvoxels,
        ) = self.__get_altered_brainvoxels_percentage(img_data_def, img_data_orig)
        
        (
            img_data_def_thresh,
            img_data_orig_thresh,
            headmask_thresh_count,
        ) = self.__get_thresholded_preliminaries(img_data_def, img_data_orig)
        del img_data_def, img_data_orig
        brain_relative_to_head = (
            totalbrainvoxels / headmask_thresh_count
        )  

        (
            percentage_headvoxels_altered_thresh_corrected,
            altered_voxels_count,
        ) = self.__get_altered_voxels_percentage(
            img_data_def_thresh, img_data_orig_thresh, headmask_thresh_count
        )
        percentage_of_completely_removed_voxels = (
            self.__get_amount_of_completely_removed_voxels(
                img_data_def_thresh, img_data_orig_thresh, headmask_thresh_count
            )
        )
        return [
            dim,
            res,
            totalbrainvoxels,
            brain_relative_to_head,
            percentage_of_brain_altered,
            altered_brain_voxels,
            altered_voxels_count,
            percentage_headvoxels_altered_thresh_corrected,
            percentage_of_completely_removed_voxels,
            self.dask_arr,
        ]

    def run_artefact_threshold_examination(self):
        print("\x1b[6;30;42m", "Processing File: ", self.def_nifti, "\x1b[0m")
        _, img_data_orig = self.__load_and_normalize()
        numb_of_voxels_orig_absolute = np.count_nonzero(img_data_orig)
        thresholds = [0.00, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.10]
        for thresh in thresholds:
            print(f"Creating headmasks for threshold {thresh}")
            head_mask = img_data_orig > thresh #binaary headmask
            #head_mask_hued_values = img_data_orig[head_mask]

            self.__create_nifti_headmask_for_manual_control(
                head_mask, f'headmask_{str(thresh).replace(".", "")}'
            )
            # for hued non binary headmask
            #self.__create_nifti_headmask_for_manual_control((head_mask_hued_values * 1000), f"headmask_original_val_hues_{str(thresh).replace('.', '')}")
            headmask_count = np.count_nonzero(head_mask)
            numb_of_headmask_voxels_deleted_by_thresh = (
                numb_of_voxels_orig_absolute - headmask_count
            )
            print(
                f"Headmask-count:{headmask_count}\n Number of headmask voxels deleted by threshold:{numb_of_headmask_voxels_deleted_by_thresh}"
            )

