import nibabel as nib
import pyvista as pv
import os
from util import config_vars
from util.io import make_dir_if_not_exists

#TODO performance optimization
class Make3DRender:
    """renders 3D Volumes of MRIs and saves them in different perspectives in 2D-images for subsequent processing.

    Returns:
        str: path to frontal image render
    """
    def __init__(self, filename) -> None:
        self.filename = filename
        if not any(
            [
                def_meth in self.filename
                for def_meth in ["mrideface", "quickshear", "pydeface"]
            ]
        ):  # TODO can put into function for readability
            self.filename_w_orig_dir = os.path.join(
                os.path.dirname(self.filename), "original", os.path.basename(filename)
            )
            self.destination_filename_prefix = (
                self.filename_w_orig_dir.replace(
                    config_vars.NIFTI_PATH, config_vars.IMAGES_PATH
                )
                .replace(".nii.gz", "")
                .replace(".nii", "")
            )
            print(self.destination_filename_prefix)
        else:
            self.destination_filename_prefix = (
                self.filename.replace(config_vars.NIFTI_PATH, config_vars.IMAGES_PATH)
                .replace(".nii.gz", "")
                .replace(".nii", "")
            )
        self.frontal_img_path = f"{self.destination_filename_prefix}_front.png"

    def __exists(self, mode):
        return os.path.isfile(f"{self.destination_filename_prefix}_{mode}.png")

    def __get_volume_and_bounds(self):  # TODO: Check again how this is working? FIXME ACHTUNG ACHTUNG HIER DASK ARRAYS WEGNEMNE?!
        """ gets the volume and bounds of the nifti array

        Returns:
            (volume, bounds): volume and bounds for visualisation
        """
        nifti_data = nib.load(self.filename)
        nifti_array = nifti_data.get_fdata() #FIXME hier kann es zum EOF Error kommen! Was bedeutet das? 
        volume = pv.UniformGrid()
        volume.dimensions = (
            nifti_array.shape
        )  # FIXME Error SetDimensions() ich denke wegen 2D #hier war vorher nur nifti array
        volume.spacing = nifti_data.header.get_zooms()
        volume.point_data["values"] = nifti_array.flatten(order="F")
        del nifti_data # not sure if necessary, but did this explicitely to save memory while usage. (just in case)
        del nifti_array
        return volume, volume.bounds

    def __make_screenshots(self):
        """saves volume as 2D images for 5 different perspectives. Uses the bounds for"""
        try:
            volume, bounds = self.__get_volume_and_bounds()
            # clim = (0,1000) oder clim = [0,256]
            pv.set_plot_theme("document")
            xmin, xmax, ymin, ymax, _, _ = bounds
            p = pv.Plotter(off_screen=True)
            # adjust image_height and width to bounds
            image_width, image_height = int((xmax - xmin) * 3), int(
                (ymax - ymin) * 3
            )

            volume.threshold(500, scalars="values", invert=True)
            p.add_volume(
                volume,
                cmap="gray",
                opacity="linear",
                clim=(0, 1000),
                resolution=[1, 1, 1],
                reset_camera=True,
            )
            p.camera_position = "yz"
            p.remove_scalar_bar()

            # TODO these could be all made in a single for loop or something similar
            p.camera.azimuth = 180
            p.camera.zoom(1.0)
            p.screenshot(
                f"{self.destination_filename_prefix}_profile_l.png",
                window_size=(image_width, image_height),
            )

            p.camera.azimuth = 135
            p.screenshot(
                f"{self.destination_filename_prefix}_angled_l.png",
                window_size=(image_width, image_height),
            )

            p.camera.azimuth = 90
            p.screenshot(
                f"{self.destination_filename_prefix}_front.png",
                window_size=(image_width, image_height),
            )

            p.camera.azimuth = 45
            p.screenshot(
                f"{self.destination_filename_prefix}_angled_r.png",
                window_size=(image_width, image_height),
            )

            p.camera.azimuth = 0
            p.screenshot(
                f"{self.destination_filename_prefix}_profile_r.png",
                window_size=(image_width, image_height),
            )  
            p.close()
            del volume
            del bounds
        except EOFError as e: 
            print('EOF-Error occured. File is likely corrupted: ', e)
            self.frontal_img_path  = 'EOFError'
            

    def render(self):
        """Final step for rendering."""
        for mode in ["angled_r", "angled_l", "front", "profile_l", "profile_r"]:
            if not self.__exists(mode):
                print('Rendering 2D Image...')
                make_dir_if_not_exists(
                    f"{os.path.dirname(self.destination_filename_prefix)}"
                )
                self.__make_screenshots()
        return self.frontal_img_path
