import step5_stats_visuals.plotting_defacing_success_and_feature_properties as plots_features
import step5_stats_visuals.cohort_stats as cohort
import step5_stats_visuals.visualisations_ML_models_results as visuals

def main():
    cohort.Plotter().all()
    plots_features.main()
    visuals.main()