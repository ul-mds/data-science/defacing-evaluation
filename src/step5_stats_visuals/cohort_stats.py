import pandas as pd
import os 
import numpy as np
import seaborn as sns
import pandas as pd 
import matplotlib.pyplot as plt
import plotly.express as px
from util import config_vars

class Plotter():
    """ Generates plots of: 
            Sessions per Subject Plot
            Age histogram
    """
    def __init__(self): # FIXME need better naming for data, and typing please maya! # evtl. hier auch aus stat alles nehmen??
        self.dicom_df = pd.read_csv('output/csv_files/raw_data_step1/filtered_dataset.csv', sep=',', low_memory=False)
        self.features_df = pd.read_csv('output/csv_files/raw_data_step4/control_merged_features_and_labels_total_Defaced (strict).csv', sep=',', low_memory=False)
        self.output_path = config_vars.VISUALS_STEP5
        self.output_path_csv = config_vars.CSV_PATH_STEP5

    def get_combined_df(self):
        self.features_df[['subject_id', 'session_id', 'scan_id']] = self.features_df['Filepath'].str.extract(r'(XNAT_MF_S\d+)_?(XNAT_MF_E\d+)_?(\w+-MR\d+|\w+)_?.*')
        self.dicom_df['scan_id'] = self.dicom_df['scan_id'].astype(str)
        self.features_df['scan_id'] = self.features_df['scan_id'].str.replace('_NIFTI', '')
        merged_df = pd.merge(self.features_df, self.dicom_df, on=['subject_id', 'session_id', 'scan_id'], how='left')
        self.dicom_df = merged_df[merged_df['Exclude']==False]

    @staticmethod
    def __conv_age_float(age_from_output:str) -> float:
        '''converts the age from this format '047Y' or '003M' to the age as float in years'''
        if 'M' in age_from_output:
            age_float = int(age_from_output.replace('M', ''))/12
            return age_float
        elif 'Y' in age_from_output:
            age_float = int(age_from_output.replace('Y', ''))
            return age_float
        else:
            raise ValueError('The entry did not have the usual format. Example of usual format: "047Y", "003M". Given input: ', age_from_output)


    @staticmethod
    def __rename_sessions(x):
        """Renames Sessions to map to a number in ascending order

        Args:
            x (_type_): _description_

        Returns:
            _type_: _description_
        """
        unique_sessions = x.unique()  # Get unique session_ids for this subject_id
        session_numbers = np.arange(1, len(unique_sessions) + 1)  # Generate unique session numbers
        session_dict = dict(zip(unique_sessions, session_numbers))  # Map session_ids to session numbers
        return x.map(session_dict)  # Return the corresponding session number for each session_id

    def age_histogram_grouped_by_sex(self, binwidth=5):
        self.dicom_df["Patient's Age"] = self.dicom_df["Patient's Age"].apply(lambda x: self.__conv_age_float(x) if not pd.isna(x) else np.nan) 
        self.dicom_df['avg_age'] = self.dicom_df.groupby('subject_id')['Patient\'s Age'].transform('mean')        
        print(self.dicom_df[['subject_id', 'avg_age',"Patient's Sex", "Patient's Age"]])
        unique_subj = self.dicom_df.drop_duplicates('subject_id', keep='first')
        unique_subj.describe().to_csv(os.path.join(config_vars.CSV_PATH_STEP5, 'DESCRIPTION_Unique_Subj_Agehist.csv'))
        means_sex = unique_subj.groupby("Patient's Sex")['avg_age'].mean()
        print(means_sex)
        overall_avg_age = unique_subj['avg_age'].mean()
        sns.set_theme(style = 'whitegrid', palette='pastel', color_codes=False)
        ax= sns.histplot(data=unique_subj,x='avg_age',hue="Patient\'s Sex",multiple='stack', binwidth=binwidth,palette={'M':'steelblue','F':'indianred'}, kde=True, alpha=0.5)
        plt.axvline(x=means_sex['F'], color='indianred', linestyle='dashed', linewidth=2, label=f'Overall Avg. Age: {overall_avg_age:.2f}')
        plt.axvline(x=means_sex['M'], color='steelblue', linestyle='dashed', linewidth=2, label=f'Overall Avg. Age: {overall_avg_age:.2f}')

        sns.set(rc={"xtick.bottom" : True, "ytick.left" : True})
        sns.set_style("whitegrid", {'axes.grid':True})
        plt.xticks([x*10 for x in range(10)])
        ax.set(xlabel='Age', ylabel='Frequency')
        ax.set_xlim(15,85)
        plt.savefig(f"{self.output_path}/seaborn_agehistogram_sexperiment.png", dpi=300)
        plt.clf()
        print('MEAN AGE TOTAL:', unique_subj['avg_age'].mean())

    def histogram_scans_and_sessions_per_subject(self) -> px.histogram:
        custom_palette = sns.light_palette("steelblue", 6).as_hex()

        df = self.dicom_df.replace('XNAT_MF_', '', regex=True) #damit SubjectID nicht so unnötiglang ist 
        df = df.drop_duplicates(subset=['subject_id', 'session_id','scan_id']) #to reduce the number, beccause now there is always 3 x because of the defacings in there. 
        subject_id_counts = df['subject_id'].value_counts().sort_values(ascending=False)
        
        subject_id_mapping = {subject_id: i + 1 for i, (subject_id, _) in enumerate(subject_id_counts.items())}
        df['numerical_subject'] = df['subject_id'].map(subject_id_mapping)
        df['session_number'] = df.groupby(['subject_id'])['session_id'].transform(self.__rename_sessions)
        df.to_csv(os.path.join(self.output_path_csv, 'Number_match.csv'))
        fig = px.histogram(
            df, 
            x="numerical_subject", 
            color="session_number", 
            color_discrete_sequence= custom_palette, 
            nbins=len(df["numerical_subject"].unique()), 
            hover_data = ['numerical_subject', 'session_id', 'session_number'], 
            labels = {'numerical_subject': 'individual subjects', 'session_id': 'Session', 'session_number': '      Session\nNumber'},
            template='plotly_white'
            )
        fig.update_layout(autosize = True, height = 1000,
                  width = 1500,
                  font=dict(size = 16),
                  margin = {'t':60, 'b':60, 'l':60})
        fig.update_layout(
            title_x=0.5,  # Set title to the center
            autosize=True,
            height=1000,
            width=1500,
            font=dict(size=30),
            margin=dict(t=80, b=100, l=60, r=60), 
            legend=dict(x=0.8, y=1) ,
        )
        tickvals = list(subject_id_mapping.values())
        fig.update_traces(marker_line_width=0.5,marker_line_color="DarkSlateGrey")
        selected_ticks = tickvals[4::5]
        fig.update_xaxes(categoryorder="total descending", tickmode='array', tickfont=dict(size=24), tickvals=selected_ticks, dtick=5, showticklabels=True, ticks='outside')
        plt.legend(loc='upper right', fontsize='xx-large', title_fontsize='xx-large' )
        fig.update_layout(
            legend=dict(orientation='h', x=0.73, y=0.95),
            legend_title=dict(side='top')
        )
        plt.tick_params(axis='both', which='both', labelsize=30)
        
        fig.update_layout(yaxis_title = 'Nr of Scans')      
        fig.update_layout(xaxis_title = 'Individual Subjects')       
        plt.xlabel('individual subjects')
        plt.margins(0.1)
        fig.write_image(f"{self.output_path}/scans_per_session_per_subject.png") 
        plt.clf()
        
    def all(self):
        self.get_combined_df()
        self.age_histogram_grouped_by_sex()
        self.histogram_scans_and_sessions_per_subject()
        descriptive_stats = self.dicom_df.describe()
        descriptive_stats.to_csv(os.path.join(config_vars.CSV_PATH_STEP5, 'FINAL_Descriptives_MERGED.csv'))
        unique_subj_id = self.dicom_df.drop_duplicates(subset='subject_id')
        sex_table = unique_subj_id["Patient's Sex"].value_counts(dropna=False)
        print(sex_table)



