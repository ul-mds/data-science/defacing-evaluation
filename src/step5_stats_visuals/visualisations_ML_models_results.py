import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


#from util.config_vars import VISUALS_PATH

def main():
    model_names = {"Bagging":0.5, "Random Forest":0.5, "Gradient Boost":0.5, "XG-Boost":0.5, "Logistic Regression":0.0}
    custom_labels=['all (16)', 'reduced (12)', 'reduced (10)', 'minimized (3)']
    feature_names = ["all_feat", "feat_red_1", "feat_red_2", "minim_feat"]
    feature_labels_mapping = dict(zip(feature_names, custom_labels))

    for model,lower_y_bound in model_names.items():
        for strictness in ['strict', 'lenient']:
            dfs = [pd.read_csv(f'output/csv_files/raw_data_step4/evaluating_model/{feat}/FINAL_complete_scores_{model}_{strictness}.csv').assign(Features=f"{feat}") for feat in feature_names]
            
            combined_df = pd.concat(dfs)
            combined_df['Features'] = combined_df['Features'].map(feature_labels_mapping)

            melted_df = combined_df.melt(id_vars=['Features'], var_name='Performance Metric', value_name='Value')

            print(f"Combined DataFrame for {model} - {strictness}:\n{combined_df}")
            custom_palette = sns.light_palette("steelblue", 6, reverse=True)

            melted_df = combined_df.melt(id_vars=['Features'], var_name = 'Performance Metric', value_name = 'Value')
            plt.figure(figsize=(15, 12))
            sns.set_style('whitegrid')

            ax = sns.pointplot(
                data=melted_df,
                x='Performance Metric',
                y='Value',
                hue='Features',
                palette=custom_palette,
                dodge=0.4,
                markers=['o', 'P', 'x','_'], 
                linestyles='', 
                errwidth=3,
                capsize=0.05, 
                errorbar='sd',
                scale=2,
            )
            plt.ylim(lower_y_bound, 1.0)
            plt.legend(title='Incl. Features (Nr)', fontsize='xx-large', title_fontsize='xx-large',loc='lower left')
            ax.grid(axis='y')
            ax.tick_params(axis='both', which='both', labelsize=26)
            ax.set(xlabel =None, ylabel=None)
            labels=["ACC", "SENS", "PREC", "F1", "SPEC", "G-MEAN"]
            plt.xticks(ticks=np.arange(len(labels)), labels=labels)
            plt.setp(ax.get_legend().get_texts(), fontsize='26')
            plt.setp(ax.get_legend().get_title(), fontsize='30')
            ax.yaxis.grid(True)  
            plt.savefig(f'output/visuals/step5/within_model_comparison_featuresets_performance_{model}_{strictness}.png', dpi=300)
            plt.close()

    model_names = ["Bagging", "Random Forest", "Gradient Boost", "XG-Boost"]
    feature_names = ["all_feat", "feat_red_1", "feat_red_2", "minim_feat"]
    #model_names_all = model_names + ["Logistic Regression"]

    for feat in feature_names:
        for strictness in ['strict', 'lenient']:
            dfs = [pd.read_csv(f'output/csv_files/raw_data_step4/evaluating_model/{feat}/FINAL_complete_scores_{model}_{strictness}.csv').assign(Model=f"{model}") for model in model_names] 
            combined_df = pd.concat(dfs)
            custom_palette = sns.light_palette("steelblue", 6, reverse=True)
            melted_df = combined_df.melt(id_vars=['Model'], var_name='Performance Metric', value_name='Value')
            plt.figure(figsize=(15, 12))
            sns.set_style('whitegrid')

            ax = sns.pointplot(
                data=melted_df,
                x='Performance Metric',
                y='Value',
                hue='Model',
                palette=custom_palette,
                dodge=0.4,
                markers=['.', 'o', '_','x', '*'], 
                linestyles='',
                errwidth=3,
                capsize=0.05, 
                errorbar='sd', 
                scale=2, 
            )
            plt.ylim(0.5, 1.01)
            legend = ax.legend(title='Model', fontsize='xx-large', title_fontsize='xx-large', loc='lower left')
            labels=["ACC", "SENS", "PREC", "F1", "SPEC", "G-MEAN"]
            plt.setp(ax.get_legend().get_texts(), fontsize='26')
            plt.setp(ax.get_legend().get_title(), fontsize='30')
            ax.grid(axis='y')
            ax.set(xlabel =None, ylabel=None)
            ax.yaxis.grid(True)  # Ensure that y-axis gridlines are drawn
            ax.tick_params(axis='both', which='both', labelsize=26)
            plt.xticks(ticks=np.arange(len(labels)), labels=labels)
            plt.savefig(f'output/visuals/step5/model_comparison_perf_metrics_no_logreg_{feat}_{strictness}.png', dpi=300)
            plt.close()

    for model in model_names:
        for strictness in ['strict', 'lenient']:
            for feat in feature_names:
                df = pd.read_csv(f'output/csv_files/raw_data_step4/evaluating_model/{feat}/FINAL_complete_scores_{model}_{strictness}.csv')
                stats = (df.describe(include='all'))
                stats.to_csv(f'output/csv_files/raw_data_step5/DESCRIPTIVES_{strictness}_{feat}_{model}.csv')
