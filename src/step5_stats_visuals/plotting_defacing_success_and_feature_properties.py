import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.ticker import FuncFormatter

def main():
    filtered_dataset_df = pd.read_csv("output/csv_files/raw_data_step1/filtered_dataset.csv")

    merged_final_csv = pd.read_csv("output/csv_files/raw_data_step4/control_merged_features_both_strictnesses.csv")
    merged_final_csv = merged_final_csv[merged_final_csv["Exclude"] == False] 

    columns_to_interpret_as_float = ['Pixel Diff','rmse','psnr','ssim','fsim','IOU OpenCV','IOU MTCNN','IOU FaceRec','Resolution','Total Nr Brain Voxels','Brain relative to head','Percentage of Brain altered','Nr of altered Brain Voxels','Nr of altered Voxels through defacing (thresh)','Percentage of Head Voxels altered through defacing (thresh)']
    for column in columns_to_interpret_as_float:
        merged_final_csv[column] = pd.to_numeric(merged_final_csv[column], errors = 'coerce')

    merged_final_csv.loc[
        merged_final_csv["Filepath"].str.contains("quickshear") == True, "Defacing Method"
    ] = 'Quickshear'
    merged_final_csv.loc[
        merged_final_csv["Filepath"].str.contains("mrideface") == True, "Defacing Method"
    ] = 'MRI-Deface'
    merged_final_csv.loc[
        merged_final_csv["Filepath"].str.contains("pydeface") == True, "Defacing Method"
    ] = 'PyDeface'

    qs_subdf = merged_final_csv.loc[merged_final_csv["Defacing Method"] == 'Quickshear']

    description_df_qs = qs_subdf.describe(include="all")
    mrid_subdf = merged_final_csv.loc[merged_final_csv["Defacing Method"] == 'MRI-Deface']

    description_df_mrid = mrid_subdf.describe(include="all")
    pydef_subdf = merged_final_csv.loc[merged_final_csv["Defacing Method"] == 'PyDeface']
    description_df_py = pydef_subdf.describe(include="all")

    description_df_mrid.to_csv("output/csv_files/raw_data_step5/DESCRIPTION_MRID.csv")
    description_df_qs.to_csv("output/csv_files/raw_data_step5/DESCRIPTION_QS.csv")
    description_df_py.to_csv("output/csv_files/raw_data_step5/DESCRIPTION_PYD.csv")

    sns.set_style('whitegrid')

    df_combined = pd.concat([merged_final_csv, merged_final_csv.copy()], ignore_index=True)
    df_combined['Strictness'] = df_combined.index < len(merged_final_csv)
    # Create a new column 'Defaced' based on 'Strictness'
    df_combined['Defaced'] = df_combined.apply(lambda row: row['Defaced (strict)'] if row['Strictness'] else row['Defaced (lenient)'], axis=1)
    # Drop the original 'Defaced (strict)' and 'Defaced (lenient)' columns
    df_combined = df_combined.drop(['Defaced (strict)', 'Defaced (lenient)'], axis=1)


    def percent_formatter(x, _):
        return f"{x:.0%}"

    sns.set_style('whitegrid')
    custom_palette = sns.light_palette("steelblue", 3, reverse=True).as_hex()

    g = sns.catplot(
        data=df_combined, kind="bar",
        x="Defacing Method", y="Defaced", hue="Strictness",
        palette=custom_palette, alpha=.9, height=6, ci=None, order=['PyDeface', 'Quickshear', 'MRI-Deface']
    )
    g.despine(left=True)
    plt.gca().yaxis.set_major_formatter(FuncFormatter(percent_formatter))
    plt.xlabel("")
    plt.ylabel("Successful Defacings", fontsize='xx-large')
    # Remove the unwanted legend
    g.fig.get_children()[-1].remove()
    g.ax.legend(loc='upper right',fontsize='x-large', title_fontsize='x-large', labels=['Lenient', 'Strict'], title='Defacing Success Criteria')
    g.ax.tick_params(axis='both', which='both', labelsize=18)
    plt.tight_layout()
    plt.savefig('output/visuals/step5/defacing_success_percentage.png', dpi=300)
    plt.close()


    merged_final_csv.loc[
        merged_final_csv["Filepath"].str.contains("t1", case = False) == True, "MRI-Modularity"
    ] = 'T1'
    merged_final_csv.loc[
        merged_final_csv["Filepath"].str.contains("t2", case = False) == True, "MRI-Modularity"
    ] = 'T2'
    merged_final_csv.loc[
        merged_final_csv["Filepath"].str.contains("flair", case = False) == True, "MRI-Modularity"
    ] = 'FLAIR'

    merged_final_csv[['subject_id', 'session_id', 'scan_id']] = merged_final_csv['Filepath'].str.extract(r'XNAT_MF_S(\d+)_XNAT_MF_E(\d+)_(\d+)_NIFTI')
    merged_final_csv['subject_id'] = 'XNAT_MF_S' + merged_final_csv['subject_id']
    merged_final_csv['session_id'] = 'XNAT_MF_E' + merged_final_csv['subject_id']

    merged_final_csv.to_csv('output/csv_files/raw_data_step5/merge_TEST.csv')
    merged_w_everything = pd.merge(filtered_dataset_df, merged_final_csv, on=['subject_id', 'session_id', 'scan_id'], how = 'right')
    merged_w_everything.to_csv('output/csv_files/raw_data_step5/merge_TEST_after_merge.csv')

    custom_palette = sns.color_palette("husl", 3)

    ax = sns.histplot(data=merged_final_csv, x='Percentage of Brain altered', hue = 'Defacing Method', bins = 100, element = 'step', palette=custom_palette)
    plt.xlabel("Perc. Brain altered", fontsize=16)
    plt.ylabel("Frequency", fontsize=16)
    ax.tick_params(axis='both', which='both', labelsize=14)
    plt.tight_layout()
    plt.savefig('output/visuals/step5/hist_brain_altered_def_meth.png', dpi=300)
    plt.clf()
    plt.close()


    ax = sns.histplot(data=merged_final_csv, x='Percentage of Brain altered', hue = 'MRI-Modularity', bins = 100, element = 'step', palette=custom_palette)
    plt.xlabel("Perc. Brain altered", fontsize=16)
    plt.ylabel("Frequency", fontsize=16)
    ax.tick_params(axis='both', which='both', labelsize=14)
    plt.tight_layout()
    plt.savefig('output/visuals/step5/hist_brain_altered_MRI-Modularity.png', dpi=300)
    plt.clf()
    plt.close()


    ax = sns.histplot(data=merged_final_csv, x='Percentage of Head Voxels altered through defacing (thresh)', hue = 'Defacing Method', bins = 100, element = 'step', palette=custom_palette)
    plt.xlabel("Perc. Head altered", fontsize=16)
    plt.ylabel("Frequency", fontsize=16)
    ax.tick_params(axis='both', which='both', labelsize=14)
    plt.tight_layout()
    plt.savefig('output/visuals/step5/hist_perc_head_altered_defmeth.png', dpi=300)
    plt.clf()
    plt.close()


    ax = sns.histplot(data=merged_final_csv, x='Percentage of Head Voxels completely removed through defacing (thresh)', hue = 'Defacing Method', bins = 100, element = 'step', palette=custom_palette, kde=True)
    plt.xlabel("Perc. Head deleted", fontsize=16)
    plt.ylabel("Frequency", fontsize=16)
    ax.tick_params(axis='both', which='both', labelsize=14)
    plt.tight_layout()
    plt.savefig('output/visuals/step5/hist_perc_head_deleted_defmeth.png', dpi=300)
    plt.clf()
    plt.close()


    ax = sns.histplot(data=merged_final_csv, x='Brain relative to head', bins = 100, element = 'step', palette=custom_palette)
    plt.xlabel("Brain to Head Ratio", fontsize=16)
    plt.ylabel("Frequency", fontsize=16)
    ax.tick_params(axis='both', which='both', labelsize=14)
    plt.tight_layout()
    plt.savefig('output/visuals/step5/hist_bhr.png', dpi=300)
    plt.clf()
    plt.close()
