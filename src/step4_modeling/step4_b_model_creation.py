
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import (
    RandomForestClassifier,
    GradientBoostingClassifier,
    BaggingClassifier,
)
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    f1_score,
    precision_score,
    recall_score,
    roc_curve,
    ConfusionMatrixDisplay,
    auc,
)
import seaborn as sns
from sklearn.inspection import permutation_importance
from sklearn.model_selection import (
    ShuffleSplit,
    StratifiedShuffleSplit,
)
from sklearn.linear_model import LogisticRegression
import xgboost as xgb

#for multicollinearity feature importance
from scipy.cluster import hierarchy
from scipy.spatial.distance import squareform
from scipy.stats import spearmanr

#from this project
from util.io import make_dir_if_not_exists
from step4_modeling.step4_a_final_dataset import CreateFinalDataset
from util import config_vars


# TODO mehr parametrisieren von paths
class BuildModels:
    def __init__(self, df, model, model_name: str, strict, balanced, feature_dir) -> None:
        self.model = model
        self.df = df
        self.strict = strict
        self.thresholds = np.linspace(0, 1, num=101)  # 0.01 steps from 0 to 1
        self.model_name = model_name
        self.n_splits = 100
        self.balanced = balanced
        self.feature_dir = feature_dir
        if balanced:
            self.cv = ShuffleSplit(n_splits=self.n_splits, test_size=0.2)
        else:
            self.cv = StratifiedShuffleSplit(n_splits=self.n_splits, test_size=0.2)
        if strict:
            self.strictness = "strict"
        else: 
            self.strictness = "lenient"
        self.VISUALS_FEATURE_IMP_PATH = os.path.join(config_vars.VISUALS_STEP4, f"feature_importances/{self.feature_dir}")
        self.CSV_MODEL_PATH = os.path.join(config_vars.CSV_PATH_STEP4,f'evaluating_model/{self.feature_dir}')
        self.VISUALS_CM = os.path.join(config_vars.VISUALS_STEP4,f"confusion_matrices/{self.feature_dir}")
        self.VISUALS_METRICS = os.path.join(config_vars.VISUALS_STEP4, f"metric_plots/{self.feature_dir}/{self.strictness}/{self.model_name}")
        make_dir_if_not_exists(self.CSV_MODEL_PATH)
        make_dir_if_not_exists(self.VISUALS_FEATURE_IMP_PATH)
        make_dir_if_not_exists(self.VISUALS_CM)
        make_dir_if_not_exists(self.VISUALS_METRICS)

    def __step1_0_threshold_finding(self, y_pred_proba, y_test) -> list:
        geom_mean_values = []
        for threshold in self.thresholds:
            y_pred_w_tresh = [
                1 if prob >= threshold else 0 for prob in y_pred_proba
            ]
            tn, fp, _, _ = confusion_matrix(y_test, y_pred_w_tresh).ravel()
            specif_thresh = tn / (tn + fp)
            sensit_thresh = recall_score(y_test, y_pred_w_tresh)
            geom_mean_thresh = np.sqrt(specif_thresh * sensit_thresh)
            geom_mean_values.append(geom_mean_thresh)
        return geom_mean_values
    
    def __plot_dendrogram(self, X):
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 8))
        corr = spearmanr(X).correlation

        corr = (corr + corr.T) / 2
        np.fill_diagonal(corr, 1)
        distance_matrix = 1 - np.abs(corr)
        dist_linkage = hierarchy.ward(squareform(distance_matrix))
        dendro = hierarchy.dendrogram(
            dist_linkage, labels=X.columns.to_list(), ax=ax1, leaf_rotation=90
        )
        dendro_idx = np.arange(0, len(dendro["ivl"]))

        im = ax2.imshow(corr[dendro["leaves"], :][:, dendro["leaves"]])
        
        # Add numbers to the heatmap squares
        for i in range(len(dendro_idx)):
            for j in range(len(dendro_idx)):
                ax2.text(j, i, f'{corr[dendro["leaves"][i], dendro["leaves"][j]]:.2f}', ha='center', va='center', color='w')
        
        ax2.set_xticks(dendro_idx)
        ax2.set_yticks(dendro_idx)
        ax2.set_xticklabels(dendro["ivl"], rotation="vertical")
        ax2.set_yticklabels(dendro["ivl"])
        _ = fig.tight_layout()
        cbar = plt.colorbar(im, ax=ax2, fraction=0.046, pad=0.04)
        cbar.set_label('Correlation')
        plt.margins(0.1)
        plt.tight_layout()
        plt.savefig(os.path.join(self.VISUALS_FEATURE_IMP_PATH, f"correlation_untersuchung_cluster_{self.feature_dir}_{self.strictness}"), dpi=300)
        ax1.clear()
        ax2.clear()
        plt.close()        

    def __step1_2_use_chosen_threshold_for_final_evaluation(self, thresh):
        X = self.df.drop("Defaced", axis=1)
        y = self.df["Defaced"]
        (
            final_accuracy_mean,
            final_recall_mean,
            final_precision_mean,
            final_f1_mean,
            final_specificity_mean,
            final_geom_mean_thresh_mean,
            cm_list, 
            importances_list_mean, importances_list_clean
        ) = ([], [], [], [], [], [], [], [], [])
        split_fct = self.cv.split(X, y)
        for _, (train_index, test_index) in enumerate(split_fct):
            #train model
            X_train, y_train = X.iloc[train_index], y.iloc[train_index]
            X_test, y_test = X.iloc[test_index], y.iloc[test_index]
            self.model.fit(X_train, y_train) 
            permutation_results = permutation_importance(self.model, X_test, y_test, n_repeats = 30)
            importances_list_mean.append(permutation_results.importances_mean)
            importances_list_clean.append(permutation_results.importances)
            y_pred_proba = self.model.predict_proba(X_test)
            y_pred_w_tresh = [1 if prob >= thresh else 0 for prob in y_pred_proba[:, 1]]

            #test model (get scores)
            final_accuracy_mean.append(accuracy_score(y_test, y_pred_w_tresh))
            final_recall_mean.append(recall_score(y_test, y_pred_w_tresh))
            final_precision_mean.append(precision_score(y_test, y_pred_w_tresh))
            final_f1_mean.append(f1_score(y_test, y_pred_w_tresh))
            tn, fp, _, tp = confusion_matrix(y_test, y_pred_w_tresh).ravel()
            final_specificity_mean.append(tn / (tn + fp))
            final_geom_mean_thresh_mean.append(
                np.sqrt(tn / (tn + fp) * recall_score(y_test, y_pred_w_tresh))
            )
            cm = confusion_matrix(y_test, y_pred_w_tresh)
            cm = cm.astype('float') / cm.sum(axis = 1)[:,np.newaxis]
            cm_list.append(cm)

        output_df_all_values = pd.DataFrame({'Accuracy':final_accuracy_mean, 'Recall':final_recall_mean, 'Precision':final_precision_mean, 'F1':final_f1_mean, 'Specificity':final_specificity_mean})
        output_df_all_values.to_csv(os.path.join(self.CSV_MODEL_PATH, f'FINAL_complete_scores_{self.model_name}_{self.strictness}.csv'), index=False)
        mean_cm = np.mean(cm_list, axis=0)
        
        ConfusionMatrixDisplay(confusion_matrix=mean_cm, display_labels=['Pass', 'Fail']).plot(cmap='Blues')
        plt.xlabel("Predicted Class")
        plt.ylabel("True Class")
        make_dir_if_not_exists(self.VISUALS_CM)
        plt.margins(0.1)
        plt.savefig(
            os.path.join(self.VISUALS_CM, f"confusionmatrix_crossval_final_{self.strictness}_{self.model_name}.png"), dpi=300)
        plt.clf()
        plt.close()
        print('Y PREDICT COMPLETE')
        mean_feature_importances_m = np.mean(importances_list_mean, axis = 0)
        mean_feature_importances_c = np.mean(importances_list_clean, axis = 0)
        output_df_all_values = pd.DataFrame({'Accuracy':final_accuracy_mean, 'Sensitivity':final_recall_mean, 'Precision':final_precision_mean, 'F1':final_f1_mean, 'Specificity':final_specificity_mean, 'G-Mean':final_geom_mean_thresh_mean})
        output_df_all_values.to_csv(os.path.join(self.CSV_MODEL_PATH, f'FINAL_complete_scores_{self.model_name}_{self.strictness}.csv'), index=False)

        sorted_indices = np.argsort(mean_feature_importances_m)
        importances = pd.DataFrame(mean_feature_importances_c[sorted_indices].T, columns=X.columns[sorted_indices])

        self.__plot_dendrogram(X)

        _, ax = plt.subplots(figsize = (28,18))
        ax = importances.plot.box(vert=False, whis=10)
        ax.axvline(x=0, color='k', linestyle='--')
        ax.set_xlabel('Decrease in Accuracy Score', fontsize=14)
        ax.tick_params(axis='both', which='both', labelsize=14)
        ax.figure.tight_layout()
        plt.margins(0.1)
        plt.savefig(os.path.join(self.VISUALS_FEATURE_IMP_PATH, f"feature_importance_permutation_{self.strictness}_{self.model_name}_meaned.png"), dpi=300)
        ax.clear()
        plt.close()
        
        print('STANDARDDEVIATION FOR',self.n_splits,'iterations:',round(np.std(final_recall_mean),3), 'STANDARD_ERROR', round(np.std(final_recall_mean)/np.sqrt(self.n_splits), 5))
        return { 
            "final_accuracy_mean": round(np.mean(final_accuracy_mean), 2),
            "final_recall_mean": round(np.mean(final_recall_mean), 2),
            "final_precision_mean": round(np.mean(final_precision_mean), 2),
            "final_specificity_mean": round(np.mean(final_specificity_mean), 2),
            "final_f1_mean": round(np.mean(final_f1_mean), 2),
            "final_geom_mean_thresh_mean": round(
                np.mean(final_geom_mean_thresh_mean), 2
            ),
        }

    def step1_monte_carlo_crossval_outer(self):
        X = self.df.drop("Defaced", axis=1)
        y = self.df["Defaced"]

        # für roc auc curve
        geom_mean_final_values, tprs_list, roc_auc_list = [], [], []
        mean_fpr = np.linspace(0, 1, 100)
        _, ax = plt.subplots(figsize=(6, 6))
        for (train_index, test_index) in self.cv.split(X, y):
            X_train, y_train = X.iloc[train_index], y.iloc[train_index]
            X_test, y_test = X.iloc[test_index], y.iloc[test_index]
            self.model.fit(X_train, y_train)
            y_pred_proba = self.model.predict_proba(X_test)[:,1]
            geom_mean_values = self.__step1_0_threshold_finding(y_pred_proba, y_test)
            geom_mean_final_values.append(geom_mean_values)
            fpr, tpr, _ = roc_curve(y_test,y_pred_proba)
            roc_auc = auc(fpr, tpr)
            tprs_list.append(np.interp(mean_fpr, fpr, tpr))
            roc_auc_list.append(roc_auc)            

        mean_geom_mean = np.mean(geom_mean_final_values, axis=0)
        mean_tpr = np.mean(tprs_list, axis = 0)
        mean_roc_auc = np.mean(roc_auc_list)
        std_auc = np.std(roc_auc_list)
        mean_tpr[0] = 0.0
        mean_tpr[-1] = 1.0 
        std_tpr = np.std(tprs_list, axis = 0)
        tprs_upper = np.minimum(mean_tpr+std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
        ax.fill_between(mean_fpr, tprs_lower, tprs_upper, color = 'grey', alpha = 0.2, label = r"$\pm$ Std. Dev.")
        plt.plot(mean_fpr, mean_tpr, label = f'Mean ROC (AUC = {mean_roc_auc:.2f} \u00B1 {std_auc:.2f})')
        plt.plot([0,1],[0,1], color='grey', linestyle='--', label = 'Chance Level (AUC = 0.5)')
        ax.set(
            xlim=[-0.05, 1.05],
            ylim=[-0.05, 1.05],
        )
        ax.set_xlabel("False Positive Rate", fontsize=16)
        ax.set_ylabel("True Positive Rate", fontsize=16)
        ax.legend(loc="lower right", fontsize='x-large')
        ax.tick_params(axis='both', which='both', labelsize=14)
        plt.tight_layout()
        plt.margins(0.1)
        plt.savefig(os.path.join(self.VISUALS_METRICS, f"ROC_CURVE_wo_extralines_{self.strictness}_{self.model_name}_montecarlo_crossval_{self.feature_dir}.png"), dpi=300)
        plt.clf()
        plt.close()
        ax.clear()

        if self.balanced:
            chosen_threshold = 0.5
        else: 
            chosen_threshold = self.__step1_1_maximize_geom_means_for_threshold(
                mean_geom_mean, "Geometric Mean of Sensitivity & Specificity"
            )
        final_return = self.__step1_2_use_chosen_threshold_for_final_evaluation(
            chosen_threshold
        )
        return (
            round(mean_roc_auc, 2),
            final_return["final_accuracy_mean"],
            final_return["final_recall_mean"],
            final_return["final_precision_mean"],
            final_return["final_specificity_mean"],
            final_return["final_f1_mean"],
            final_return["final_geom_mean_thresh_mean"],
            chosen_threshold,
        )

    def __step1_1_maximize_geom_means_for_threshold(
        self, mean_score, lab: str
    ):  
        plt.plot(self.thresholds, mean_score, label=lab, linewidth=3, zorder=1)
        max_index = np.argmax(mean_score)
        max_indices = np.where(mean_score == np.max(mean_score))[0]
        thresholds_to_chose_from = []
        for max_index in max_indices:
            max_x = self.thresholds[max_index]
            max_y = mean_score[max_index]
            thresholds_to_chose_from.append(max_x)
            plt.scatter(max_x, max_y, color="black", marker="+", zorder=3)
        list_of_diff_to_medium = [
            abs(thresh - 0.5) for thresh in thresholds_to_chose_from
        ]
        min_index = np.argmin(list_of_diff_to_medium)
        plt.scatter(
            max_x, max_y, color="black", marker="P", label="Maximum Value(s)", zorder=3, s = 100, 
        )
        plt.xlabel("Threshold", fontsize= 20)
        plt.ylabel("G-Mean", fontsize= 20)
        plt.grid(True)
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)
        plt.legend(loc='lower right', fontsize='xx-large')
        plt.tight_layout()
        plt.margins(0.1)
        plt.savefig(os.path.join(self.VISUALS_METRICS,f"tuning_threshold_plot_{self.strictness}_{lab}_{self.feature_dir}.png" ), dpi=300)
        plt.clf()
        plt.close()
        return thresholds_to_chose_from[min_index]

def get_corr_matrix():
    for strictness in [True, False]:
        for feature_mode in ['all_feat', 'feat_red_1', 'feat_red_2','minim_feat']:
            df, _ = CreateFinalDataset(strictness).run(feature_mode)
            correlation_matrix = df.corr(method = 'spearman')
            plt.figure(figsize=(12, 10))
            sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', linewidths=.5, fmt = '.2f')

            plt.xticks(
                np.arange(len(correlation_matrix.columns))+0.5,
                correlation_matrix.columns,
                ha="center",
            )
            plt.yticks(
                np.arange(len(correlation_matrix.columns))+0.5,
                correlation_matrix.columns,
                va="center"  
            )
            plt.tight_layout()
            plt.margins(0.1)
            if strictness:
                plt.savefig(os.path.join(config_vars.VISUALS_STEP4,f"correlation_heatmap_strict_{feature_mode}.png"), dpi=300)
            else:
                plt.savefig(os.path.join(config_vars.VISUALS_STEP4,f"correlation_heatmap_lenient_{feature_mode}.png"), dpi=300)
            plt.clf()

def main():
    get_corr_matrix() 
    models = {
        "Logistic Regression": LogisticRegression(),
        "Bagging": BaggingClassifier(),
        "Random Forest": RandomForestClassifier(),
        "Gradient Boost": GradientBoostingClassifier(),
        "XG-Boost": xgb.XGBClassifier(objective="binary:logistic"),
    }
    columns = [
        "Model",
        "Strictness",
        "MEAN AUC",
        "Accuracy",
        "Recall/Sensitivity",
        "Precision",
        "Specificity",
        "F1",
        "Geom-Mean of Sens&Spec",
    ]

    for feature_mode in ['all_feat', 'feat_red_1', 'feat_red_2','minim_feat']: 
        df_for_table = pd.DataFrame(columns=columns)

        for strictness in [True, False]:
            df, balanced = CreateFinalDataset(strictness).run(feature_mode)
            for (
                key,
                value,
            ) in (
                models.items()
            ):  
                (
                    mean_auc,
                    final_accuracy,
                    final_recall,
                    final_precision,
                    final_specificity,
                    final_f1,
                    final_geom_mean_thresh,chosen_threshold,
                ) = BuildModels(df, value, key, strictness, balanced, feature_mode).step1_monte_carlo_crossval_outer()
                df_for_table = df_for_table.append(
                    {
                        "Model": key,
                        "Strictness": strictness,
                        "MEAN AUC": mean_auc,
                        "Accuracy": final_accuracy,
                        "Recall/Sensitivity": final_recall,
                        "Precision": final_precision,
                        "Specificity": final_specificity,
                        "F1": final_f1,
                        "Geom-Mean of Sens&Spec": final_geom_mean_thresh,
                        "classification threshold": chosen_threshold,
                    },
                    ignore_index=True,
                )
        df_for_table.to_csv(os.path.join(config_vars.CSV_PATH_STEP4, f"evaluating_model/{feature_mode}/FINAL_RESULTS_{feature_mode}.csv"))