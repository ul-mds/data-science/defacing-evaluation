import pandas as pd
import numpy as np
import os
from util import config_vars

# TODO die ganzen csv namen parametrisieren

class CreateFinalDataset:
    def __init__(
        self,
        strict=True,
        eval_csv=os.path.join(
            config_vars.CSV_PATH_STEP3, "manual_evaluation_FINALVERSION.csv"
        ),
        feature_csv=os.path.join(
            config_vars.CSV_PATH_STEP3, "feature_collection_results.csv"
        ),
        duplicate_csv=os.path.join(
            config_vars.CSV_PATH_STEP1, "duplicate_files_nodefacedones.csv"
        ),
    ) -> None:
        self.eval_df = pd.read_csv(eval_csv).reset_index(drop=True)
        self.feature_df = pd.read_csv(feature_csv).reset_index(drop=True)
        self.duplicate_df = pd.read_csv(duplicate_csv)
        self.strict = strict
        if strict:
            self.varname_to_delete = "Defaced (lenient)"
            self.strictness_var_name = "Defaced (strict)"
        else:
            self.varname_to_delete = "Defaced (strict)"
            self.strictness_var_name = "Defaced (lenient)"

    def step1_merge_df(self):
        # for good merging, the nii.log needs to be nii.gz #FIXME IN other script!
        self.eval_df["Filepath"] = self.eval_df["Filepath"].str.replace(
            ".nii.log", ".nii.gz"
        )  # bessere lösung im code direkt vorher machen
        df_merged = pd.merge(
            self.eval_df,
            self.feature_df,
            left_on="Filepath",
            right_on="Defaced Nifti",
            how="outer",
        )
        print(df_merged.columns)
        return df_merged

    def step2_delete_duplicates(self, df_merged):
        duplicates = self.duplicate_df["Duplicate"]
        mask = df_merged["Original Nifti"].isin(duplicates)
        # duplicates already handled in step1 now, thus no problem now.
        print("shape PRIOR TO DUPLICATE DELETION")
        print(df_merged.shape)
        # Use the mask to filter 'df_features' and keep only the rows that don't match with 'duplicates'
        df_merged = df_merged[
            ~mask
        ]  # sollte es nicht an feature_df machen? Oder doch! Weil nur darin dieses dings drin ist. oder im merged one?! prior to column deletion.
        print("shape AFTER DUPLICATE DELETION")
        print(df_merged.shape)
        return df_merged

    def step3_delete_certain_rows(self, df_merged):
        # lösche die zeilen wo keine übereinstimmung ist
        # i think the merge was not successful
        print("Shape before dropna", df_merged.shape)
        df_merged.dropna(
            subset=["Filepath", "Original Nifti"], inplace=True
        )  # prüfe wie viele zeilen und welche das betrifft: 3 TODO
        ##rows_with_nan = df[df.isna().any(axis=1)] #TODO!?
        print("Shape before deletion of excluded", df_merged.shape)
        df_merged = df_merged[df_merged["Exclude"] == False]  # removes 154 rows.
        # lösche Zeilen mit NAs
        print("Shape before dropping NAs", df_merged.shape)
        df_merged.dropna()  # KLAPPT DAS?! #löscht keine weiteren Zeilen. Sonst gibt es keine NAs <3
        print("Shape after dropna", df_merged.shape)
        return df_merged

    def step4_delete_certain_columns(self, df_merged):  # metadata
        # drop columns not necessary for the training of the model. like metadata i just had for myself
        df_dropped_cols = df_merged.drop(
            [
                "Filepath",
                "Filepath Image",
                "Mouth",
                "Nose",
                "Eyes",
                "Jaw/Facestructure",
                "Revisit",
                "Exclude",
                "Original Nifti",
                "Defaced Nifti",
                "Frontal Orig Img",
                "Frontal Def Img",
                "Nifti-Dimensions",
                "Comments",
                "Dask Array used",
            ],
            axis=1,
        )
        df_dropped_cols = df_dropped_cols.drop(
            self.varname_to_delete, axis=1
        )  # ah das hier kommt zu columns deletion
        print("SHAPE OF DATAFRAME AFTER DELETION OF COLUMNS: ", df_dropped_cols.shape)
        return df_dropped_cols

    def step5_numeric_adjustments(self, df):
        # replacing infinity with a very high number like 100000
        df = df.apply(pd.to_numeric, errors="raise") 
        df.replace(
            [np.inf, -np.inf], 100000, inplace=True
        )  
        # make whole dataframe numeric
        # bad case = defacing insufficient = False = 1 ; good case = defacing good = True = 0 
        df[self.strictness_var_name] = df[self.strictness_var_name].map(
            {True: 0, False: 1}
        )  
        return df

    def step6_create_csvs_for_checkups(  # TODO irgendwann kann das hier denke ich gelöscht werden..
        self, df, naming_of_csv
    ):  # not sure if necessary
        # zu csv um zu überprüfen ob es apsst
        df.to_csv(f"{config_vars.CSV_PATH_STEP4}/{naming_of_csv}.csv", index=False)

    #for potential future purpose of trying out per defacing method alone. NOT USED FOR NOW.
    def run_seperate_defacing_methods(self, df):
        df.loc[
            df["Filepath"].str.contains("quickshear") == True, "DEFACING METHOD"
        ] = "Quickshear"
        df.loc[
            df["Filepath"].str.contains("mrideface") == True, "DEFACING METHOD"
        ] = "MRI-Deface"
        df.loc[
            df["Filepath"].str.contains("pydeface") == True, "DEFACING METHOD"
        ] = "Pydeface"
        print(df[["Filepath", "DEFACING METHOD"]])

        qs_subdf = df.loc[df["DEFACING METHOD"] == "Quickshear"]
        mrid_subdf = df.loc[df["DEFACING METHOD"] == "MRI-Deface"]
        pydef_subdf = df.loc[df["DEFACING METHOD"] == "Pydeface"]
        return qs_subdf, mrid_subdf, pydef_subdf

    def run(
        self, feature_mode="all_feat"
    ):  # features_voxel_based=0, features_sim_based = 0
        print("eval shape", self.eval_df.shape)
        print("feature shape", self.feature_df.shape)
        # erst zusammenführen
        df_merged = self.step1_merge_df()
        print("merged shape: ", df_merged.shape)
        # dann duplikate löschen
        merged_df_noduplicates = self.step2_delete_duplicates(df_merged)
        # dann rows und columns löschen
        df_merged_wo_rows = self.step3_delete_certain_rows(merged_df_noduplicates)
        self.step6_create_csvs_for_checkups(
            df_merged,
            f"control_merged_features_both_strictnesses",
        )
        df_merged_dropped_cols = self.step4_delete_certain_columns(df_merged_wo_rows)
        df_final = self.step5_numeric_adjustments(df_merged_dropped_cols)
        # df_final = df_final.reset_index(drop=True)

        self.step6_create_csvs_for_checkups(
            df_merged,
            f"control_merged_features_and_labels_total_{self.strictness_var_name}",
        )
        self.step6_create_csvs_for_checkups(
            df_final, f"control_final_df_for_model_{self.strictness_var_name}"
        )

        df_final = df_final.rename(
            columns={self.strictness_var_name: "Defaced"}
        )  # renaming damit einheitlich von nun an für weitere verfahren.

        #Balance evaluation 
        balance_distribution = df_final["Defaced"].value_counts()
        balance_ratio = balance_distribution[0] / (
            balance_distribution[1] + balance_distribution[0]
        )
        print(balance_ratio)
        if 0.4 < balance_ratio < 0.6:
            print("BALANCED")
            balanced = True
        else:
            print("IMBALANCED")
            balanced = False
        print("Balance Check \n", balance_distribution)

        #for shorter naming, TODO should be handled in the long run from the beginning
        column_mapping = {
            "Percentage of Head Voxels altered through defacing (thresh)": "Perc. Head altered",
            "Percentage of Head Voxels completely removed through defacing (thresh)": "Perc. Head deleted",
            "Total Nr Brain Voxels": "Nr BV",
            "Brain relative to head": "BHR",  # brain to head ratio
            "Percentage of Brain altered": "Perc. Brain alteration",
            "Nr of altered Brain Voxels": "Nr altered BV",
            "Nr of altered Voxels through defacing (thresh)": "Nr Voxel alteration",
        }
        df_final.rename(columns=column_mapping, inplace=True)

        # feature minimization
        final_df_output = df_final
        if feature_mode == "all_feat":
            final_df_output = df_final
        elif feature_mode == "feat_red_1":
            keep_list = [
                "Defaced",
                "fsim",
                "ssim",
                "BHR",
                #"IOU OpenCV",
                "Perc. Head deleted",
                "IOU FaceRec",
                "Nr BV",
                "Perc. Brain alteration",
                "Perc. Head altered",
                "Nr Voxel alteration",
                #"Nr altered BV",
                #"rmse",
                #"psnr",---
                "IOU MTCNN",
                "Resolution",
                "Pixel Diff", 
            ]  
            final_df_output = df_final[keep_list]

        elif feature_mode == "feat_red_2":
            if self.strict:
                keep_list = [
                    "Defaced",
                    "fsim",
                    "ssim",
                    "BHR",
                    #"IOU OpenCV",
                    "Perc. Head deleted",
                    #"IOU FaceRec",
                    "Nr BV",
                    "Perc. Brain alteration",
                    "Perc. Head altered",
                    "Nr Voxel alteration",
                    #"Nr altered BV",
                    #"rmse",
                    #"psnr",---
                    "IOU MTCNN",
                    #"Resolution",
                    "Pixel Diff", 
                ] 
            else:
                keep_list = [
                    "Defaced",
                    "fsim",
                    "ssim",
                    #"BHR",
                    #"IOU OpenCV",
                    "Perc. Head deleted",
                    "IOU FaceRec",
                    "Nr BV",
                    "Perc. Brain alteration",
                    "Perc. Head altered",
                    "Nr Voxel alteration",
                    #"Nr altered BV",
                    #"rmse",
                    #"psnr",---
                    #"IOU MTCNN",
                    "Resolution",
                    "Pixel Diff", 
                ] 
            final_df_output = df_final[keep_list]

        elif feature_mode == "minim_feat":
            keep_list = [
                "Defaced",
                "fsim",
                "Perc. Head deleted",
                "Perc. Brain alteration",
            ]
            final_df_output = df_final[keep_list]
        return final_df_output, balanced
